chrome.extension.onConnect.addListener(function(port) {
  var extensionListener = function(message, sender, sendResponse) {
    if (message.action === "getDOM") {
      chrome.tabs.sendMessage(message.tabId, message, sendResponse)
    } else if (message.action === "sendDOM") {
      port.postMessage(message)
    }
  }

  // Listens to messages sent from the panel
  chrome.extension.onMessage.addListener(extensionListener)

  port.onDisconnect.addListener(function(port) {
    chrome.extension.onMessage.removeListener(extensionListener)
  })
})

// chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
// 		console.log("chrome.runtime.onMessage", request);
//     return true;
// });
