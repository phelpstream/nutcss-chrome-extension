(function(){function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s}return e})()({1:[function(require,module,exports){
(function (global){
const frame=require("./frame");window.nutcssFrame=frame,global.nutcssFrame=frame;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./frame":2}],2:[function(require,module,exports){
const parse=require("./parse"),frame={frame:{alt:["parse","scrape"],fn:function(e,r,a=!1){return e.value(parse(e,r,a)),e}}};module.exports=frame;

},{"./parse":356}],3:[function(require,module,exports){
var always=require("./always"),F=always(!1);module.exports=F;

},{"./always":11}],4:[function(require,module,exports){
var always=require("./always"),T=always(!0);module.exports=T;

},{"./always":11}],5:[function(require,module,exports){
module.exports={"@@functional/placeholder":!0};

},{}],6:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),add=_curry2(function(r,u){return Number(r)+Number(u)});module.exports=add;

},{"./internal/_curry2":108}],7:[function(require,module,exports){
var _concat=require("./internal/_concat"),_curry1=require("./internal/_curry1"),curryN=require("./curryN"),addIndex=_curry1(function(r){return curryN(r.length,function(){var n=0,e=arguments[0],t=arguments[arguments.length-1],c=Array.prototype.slice.call(arguments,0);return c[0]=function(){var r=e.apply(this,_concat(arguments,[n,t]));return n+=1,r},r.apply(this,c)})});module.exports=addIndex;

},{"./curryN":44,"./internal/_concat":103,"./internal/_curry1":107}],8:[function(require,module,exports){
var _concat=require("./internal/_concat"),_curry3=require("./internal/_curry3"),adjust=_curry3(function(r,n,t){if(n>=t.length||n<-t.length)return t;var e=(n<0?t.length:0)+n,u=_concat(t);return u[e]=r(t[e]),u});module.exports=adjust;

},{"./internal/_concat":103,"./internal/_curry3":109}],9:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xall=require("./internal/_xall"),all=_curry2(_dispatchable(["all"],_xall,function(r,l){for(var a=0;a<l.length;){if(!r(l[a]))return!1;a+=1}return!0}));module.exports=all;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xall":145}],10:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),curryN=require("./curryN"),max=require("./max"),pluck=require("./pluck"),reduce=require("./reduce"),allPass=_curry1(function(r){return curryN(reduce(max,0,pluck("length",r)),function(){for(var e=0,u=r.length;e<u;){if(!r[e].apply(this,arguments))return!1;e+=1}return!0})});module.exports=allPass;

},{"./curryN":44,"./internal/_curry1":107,"./max":196,"./pluck":241,"./reduce":252}],11:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),always=_curry1(function(r){return function(){return r}});module.exports=always;

},{"./internal/_curry1":107}],12:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),and=_curry2(function(r,n){return r&&n});module.exports=and;

},{"./internal/_curry2":108}],13:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xany=require("./internal/_xany"),any=_curry2(_dispatchable(["any"],_xany,function(r,a){for(var e=0;e<a.length;){if(r(a[e]))return!0;e+=1}return!1}));module.exports=any;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xany":146}],14:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),curryN=require("./curryN"),max=require("./max"),pluck=require("./pluck"),reduce=require("./reduce"),anyPass=_curry1(function(r){return curryN(reduce(max,0,pluck("length",r)),function(){for(var e=0,u=r.length;e<u;){if(r[e].apply(this,arguments))return!0;e+=1}return!1})});module.exports=anyPass;

},{"./curryN":44,"./internal/_curry1":107,"./max":196,"./pluck":241,"./reduce":252}],15:[function(require,module,exports){
var _concat=require("./internal/_concat"),_curry2=require("./internal/_curry2"),_reduce=require("./internal/_reduce"),map=require("./map"),ap=_curry2(function(n,r){return"function"==typeof r["fantasy-land/ap"]?r["fantasy-land/ap"](n):"function"==typeof n.ap?n.ap(r):"function"==typeof n?function(e){return n(e)(r(e))}:_reduce(function(n,e){return _concat(n,map(e,r))},[],n)});module.exports=ap;

},{"./internal/_concat":103,"./internal/_curry2":108,"./internal/_reduce":140,"./map":190}],16:[function(require,module,exports){
var _aperture=require("./internal/_aperture"),_curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xaperture=require("./internal/_xaperture"),aperture=_curry2(_dispatchable([],_xaperture,_aperture));module.exports=aperture;

},{"./internal/_aperture":95,"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xaperture":147}],17:[function(require,module,exports){
var _concat=require("./internal/_concat"),_curry2=require("./internal/_curry2"),append=_curry2(function(r,n){return _concat(n,[r])});module.exports=append;

},{"./internal/_concat":103,"./internal/_curry2":108}],18:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),apply=_curry2(function(r,p){return r.apply(this,p)});module.exports=apply;

},{"./internal/_curry2":108}],19:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),apply=require("./apply"),curryN=require("./curryN"),map=require("./map"),max=require("./max"),pluck=require("./pluck"),reduce=require("./reduce"),values=require("./values"),applySpec=_curry1(function r(e){return e=map(function(e){return"function"==typeof e?e:r(e)},e),curryN(reduce(max,0,pluck("length",values(e))),function(){var r=arguments;return map(function(e){return apply(e,r)},e)})});module.exports=applySpec;

},{"./apply":18,"./curryN":44,"./internal/_curry1":107,"./map":190,"./max":196,"./pluck":241,"./reduce":252,"./values":311}],20:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),applyTo=_curry2(function(r,u){return u(r)});module.exports=applyTo;

},{"./internal/_curry2":108}],21:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),ascend=_curry3(function(r,e,n){var u=r(e),c=r(n);return u<c?-1:u>c?1:0});module.exports=ascend;

},{"./internal/_curry3":109}],22:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),assoc=_curry3(function(r,u,a){var c={};for(var e in a)c[e]=a[e];return c[r]=u,c});module.exports=assoc;

},{"./internal/_curry3":109}],23:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),_has=require("./internal/_has"),_isArray=require("./internal/_isArray"),_isInteger=require("./internal/_isInteger"),assoc=require("./assoc"),isNil=require("./isNil"),assocPath=_curry3(function r(e,i,s){if(0===e.length)return i;var a=e[0];if(e.length>1){var n=!isNil(s)&&_has(a,s)?s[a]:_isInteger(e[1])?[]:{};i=r(Array.prototype.slice.call(e,1),i,n)}if(_isInteger(a)&&_isArray(s)){var t=[].concat(s);return t[a]=i,t}return assoc(a,i,s)});module.exports=assocPath;

},{"./assoc":22,"./internal/_curry3":109,"./internal/_has":119,"./internal/_isArray":123,"./internal/_isInteger":126,"./isNil":174}],24:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),nAry=require("./nAry"),binary=_curry1(function(r){return nAry(2,r)});module.exports=binary;

},{"./internal/_curry1":107,"./nAry":214}],25:[function(require,module,exports){
var _arity=require("./internal/_arity"),_curry2=require("./internal/_curry2"),bind=_curry2(function(r,n){return _arity(r.length,function(){return r.apply(n,arguments)})});module.exports=bind;

},{"./internal/_arity":96,"./internal/_curry2":108}],26:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_isFunction=require("./internal/_isFunction"),and=require("./and"),lift=require("./lift"),both=_curry2(function(r,i){return _isFunction(r)?function(){return r.apply(this,arguments)&&i.apply(this,arguments)}:lift(and)(r,i)});module.exports=both;

},{"./and":12,"./internal/_curry2":108,"./internal/_isFunction":125,"./lift":186}],27:[function(require,module,exports){
var curry=require("./curry"),call=curry(function(r){return r.apply(this,Array.prototype.slice.call(arguments,1))});module.exports=call;

},{"./curry":43}],28:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_makeFlat=require("./internal/_makeFlat"),_xchain=require("./internal/_xchain"),map=require("./map"),chain=_curry2(_dispatchable(["fantasy-land/chain","chain"],_xchain,function(a,r){return"function"==typeof r?function(e){return a(r(e))(e)}:_makeFlat(!1)(map(a,r))}));module.exports=chain;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_makeFlat":133,"./internal/_xchain":148,"./map":190}],29:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),clamp=_curry3(function(r,e,n){if(r>e)throw new Error("min must not be greater than max in clamp(min, max, value)");return n<r?r:n>e?e:n});module.exports=clamp;

},{"./internal/_curry3":109}],30:[function(require,module,exports){
var _clone=require("./internal/_clone"),_curry1=require("./internal/_curry1"),clone=_curry1(function(e){return null!=e&&"function"==typeof e.clone?e.clone():_clone(e,[],[],!0)});module.exports=clone;

},{"./internal/_clone":100,"./internal/_curry1":107}],31:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),comparator=_curry1(function(r){return function(u,n){return r(u,n)?-1:r(n,u)?1:0}});module.exports=comparator;

},{"./internal/_curry1":107}],32:[function(require,module,exports){
var lift=require("./lift"),not=require("./not"),complement=lift(not);module.exports=complement;

},{"./lift":186,"./not":217}],33:[function(require,module,exports){
var pipe=require("./pipe"),reverse=require("./reverse");function compose(){if(0===arguments.length)throw new Error("compose requires at least one argument");return pipe.apply(this,reverse(arguments))}module.exports=compose;

},{"./pipe":238,"./reverse":261}],34:[function(require,module,exports){
var chain=require("./chain"),compose=require("./compose"),map=require("./map");function composeK(){if(0===arguments.length)throw new Error("composeK requires at least one argument");var e=Array.prototype.slice.call(arguments),o=e.pop();return compose(compose.apply(this,map(chain,e)),o)}module.exports=composeK;

},{"./chain":28,"./compose":33,"./map":190}],35:[function(require,module,exports){
var pipeP=require("./pipeP"),reverse=require("./reverse");function composeP(){if(0===arguments.length)throw new Error("composeP requires at least one argument");return pipeP.apply(this,reverse(arguments))}module.exports=composeP;

},{"./pipeP":240,"./reverse":261}],36:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_isArray=require("./internal/_isArray"),_isFunction=require("./internal/_isFunction"),_isString=require("./internal/_isString"),toString=require("./toString"),concat=_curry2(function(r,n){if(_isArray(r)){if(_isArray(n))return r.concat(n);throw new TypeError(toString(n)+" is not an array")}if(_isString(r)){if(_isString(n))return r+n;throw new TypeError(toString(n)+" is not a string")}if(null!=r&&_isFunction(r["fantasy-land/concat"]))return r["fantasy-land/concat"](n);if(null!=r&&_isFunction(r.concat))return r.concat(n);throw new TypeError(toString(r)+' does not have a method named "concat" or "fantasy-land/concat"')});module.exports=concat;

},{"./internal/_curry2":108,"./internal/_isArray":123,"./internal/_isFunction":125,"./internal/_isString":131,"./toString":289}],37:[function(require,module,exports){
var _arity=require("./internal/_arity"),_curry1=require("./internal/_curry1"),map=require("./map"),max=require("./max"),reduce=require("./reduce"),cond=_curry1(function(r){var e=reduce(max,0,map(function(r){return r[0].length},r));return _arity(e,function(){for(var e=0;e<r.length;){if(r[e][0].apply(this,arguments))return r[e][1].apply(this,arguments);e+=1}})});module.exports=cond;

},{"./internal/_arity":96,"./internal/_curry1":107,"./map":190,"./max":196,"./reduce":252}],38:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),constructN=require("./constructN"),construct=_curry1(function(r){return constructN(r.length,r)});module.exports=construct;

},{"./constructN":39,"./internal/_curry1":107}],39:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),curry=require("./curry"),nAry=require("./nAry"),constructN=_curry2(function(r,e){if(r>10)throw new Error("Constructor with greater than ten arguments");return 0===r?function(){return new e}:curry(nAry(r,function(r,n,t,u,c,s,a,w,o,i){switch(arguments.length){case 1:return new e(r);case 2:return new e(r,n);case 3:return new e(r,n,t);case 4:return new e(r,n,t,u);case 5:return new e(r,n,t,u,c);case 6:return new e(r,n,t,u,c,s);case 7:return new e(r,n,t,u,c,s,a);case 8:return new e(r,n,t,u,c,s,a,w);case 9:return new e(r,n,t,u,c,s,a,w,o);case 10:return new e(r,n,t,u,c,s,a,w,o,i)}}))});module.exports=constructN;

},{"./curry":43,"./internal/_curry2":108,"./nAry":214}],40:[function(require,module,exports){
var _contains=require("./internal/_contains"),_curry2=require("./internal/_curry2"),contains=_curry2(_contains);module.exports=contains;

},{"./internal/_contains":104,"./internal/_curry2":108}],41:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_map=require("./internal/_map"),curryN=require("./curryN"),max=require("./max"),pluck=require("./pluck"),reduce=require("./reduce"),converge=_curry2(function(r,e){return curryN(reduce(max,0,pluck("length",e)),function(){var u=arguments,c=this;return r.apply(c,_map(function(r){return r.apply(c,u)},e))})});module.exports=converge;

},{"./curryN":44,"./internal/_curry2":108,"./internal/_map":134,"./max":196,"./pluck":241,"./reduce":252}],42:[function(require,module,exports){
var reduceBy=require("./reduceBy"),countBy=reduceBy(function(e,r){return e+1},0);module.exports=countBy;

},{"./reduceBy":253}],43:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),curryN=require("./curryN"),curry=_curry1(function(r){return curryN(r.length,r)});module.exports=curry;

},{"./curryN":44,"./internal/_curry1":107}],44:[function(require,module,exports){
var _arity=require("./internal/_arity"),_curry1=require("./internal/_curry1"),_curry2=require("./internal/_curry2"),_curryN=require("./internal/_curryN"),curryN=_curry2(function(r,u){return 1===r?_curry1(u):_arity(r,_curryN(r,[],u))});module.exports=curryN;

},{"./internal/_arity":96,"./internal/_curry1":107,"./internal/_curry2":108,"./internal/_curryN":110}],45:[function(require,module,exports){
var add=require("./add"),dec=add(-1);module.exports=dec;

},{"./add":6}],46:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),defaultTo=_curry2(function(r,u){return null==u||u!=u?r:u});module.exports=defaultTo;

},{"./internal/_curry2":108}],47:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),descend=_curry3(function(r,e,n){var u=r(e),c=r(n);return u>c?-1:u<c?1:0});module.exports=descend;

},{"./internal/_curry3":109}],48:[function(require,module,exports){
var _contains=require("./internal/_contains"),_curry2=require("./internal/_curry2"),difference=_curry2(function(r,n){for(var e=[],i=0,t=r.length;i<t;)_contains(r[i],n)||_contains(r[i],e)||(e[e.length]=r[i]),i+=1;return e});module.exports=difference;

},{"./internal/_contains":104,"./internal/_curry2":108}],49:[function(require,module,exports){
var _containsWith=require("./internal/_containsWith"),_curry3=require("./internal/_curry3"),differenceWith=_curry3(function(r,n,i){for(var e=[],t=0,c=n.length;t<c;)_containsWith(r,n[t],i)||_containsWith(r,n[t],e)||e.push(n[t]),t+=1;return e});module.exports=differenceWith;

},{"./internal/_containsWith":105,"./internal/_curry3":109}],50:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),dissoc=_curry2(function(r,e){var u={};for(var c in e)u[c]=e[c];return delete u[r],u});module.exports=dissoc;

},{"./internal/_curry2":108}],51:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_isInteger=require("./internal/_isInteger"),assoc=require("./assoc"),dissoc=require("./dissoc"),remove=require("./remove"),update=require("./update"),dissocPath=_curry2(function e(r,s){switch(r.length){case 0:return s;case 1:return _isInteger(r[0])?remove(r[0],1,s):dissoc(r[0],s);default:var t=r[0],i=Array.prototype.slice.call(r,1);return null==s[t]?s:_isInteger(r[0])?update(t,e(i,s[t]),s):assoc(t,e(i,s[t]),s)}});module.exports=dissocPath;

},{"./assoc":22,"./dissoc":50,"./internal/_curry2":108,"./internal/_isInteger":126,"./remove":258,"./update":309}],52:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),divide=_curry2(function(r,e){return r/e});module.exports=divide;

},{"./internal/_curry2":108}],53:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xdrop=require("./internal/_xdrop"),slice=require("./slice"),drop=_curry2(_dispatchable(["drop"],_xdrop,function(r,e){return slice(Math.max(0,r),1/0,e)}));module.exports=drop;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xdrop":149,"./slice":265}],54:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_dropLast=require("./internal/_dropLast"),_xdropLast=require("./internal/_xdropLast"),dropLast=_curry2(_dispatchable([],_xdropLast,_dropLast));module.exports=dropLast;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_dropLast":112,"./internal/_xdropLast":150}],55:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_dropLastWhile=require("./internal/_dropLastWhile"),_xdropLastWhile=require("./internal/_xdropLastWhile"),dropLastWhile=_curry2(_dispatchable([],_xdropLastWhile,_dropLastWhile));module.exports=dropLastWhile;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_dropLastWhile":113,"./internal/_xdropLastWhile":151}],56:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_dispatchable=require("./internal/_dispatchable"),_xdropRepeatsWith=require("./internal/_xdropRepeatsWith"),dropRepeatsWith=require("./dropRepeatsWith"),equals=require("./equals"),dropRepeats=_curry1(_dispatchable([],_xdropRepeatsWith(equals),dropRepeatsWith(equals)));module.exports=dropRepeats;

},{"./dropRepeatsWith":57,"./equals":64,"./internal/_curry1":107,"./internal/_dispatchable":111,"./internal/_xdropRepeatsWith":152}],57:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xdropRepeatsWith=require("./internal/_xdropRepeatsWith"),last=require("./last"),dropRepeatsWith=_curry2(_dispatchable([],_xdropRepeatsWith,function(e,r){var t=[],a=1,i=r.length;if(0!==i)for(t[0]=r[0];a<i;)e(last(t),r[a])||(t[t.length]=r[a]),a+=1;return t}));module.exports=dropRepeatsWith;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xdropRepeatsWith":152,"./last":179}],58:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xdropWhile=require("./internal/_xdropWhile"),slice=require("./slice"),dropWhile=_curry2(_dispatchable(["dropWhile"],_xdropWhile,function(r,e){for(var i=0,l=e.length;i<l&&r(e[i]);)i+=1;return slice(i,1/0,e)}));module.exports=dropWhile;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xdropWhile":153,"./slice":265}],59:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_isFunction=require("./internal/_isFunction"),lift=require("./lift"),or=require("./or"),either=_curry2(function(r,i){return _isFunction(r)?function(){return r.apply(this,arguments)||i.apply(this,arguments)}:lift(or)(r,i)});module.exports=either;

},{"./internal/_curry2":108,"./internal/_isFunction":125,"./lift":186,"./or":225}],60:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_isArguments=require("./internal/_isArguments"),_isArray=require("./internal/_isArray"),_isObject=require("./internal/_isObject"),_isString=require("./internal/_isString"),empty=_curry1(function(t){return null!=t&&"function"==typeof t["fantasy-land/empty"]?t["fantasy-land/empty"]():null!=t&&null!=t.constructor&&"function"==typeof t.constructor["fantasy-land/empty"]?t.constructor["fantasy-land/empty"]():null!=t&&"function"==typeof t.empty?t.empty():null!=t&&null!=t.constructor&&"function"==typeof t.constructor.empty?t.constructor.empty():_isArray(t)?[]:_isString(t)?"":_isObject(t)?{}:_isArguments(t)?function(){return arguments}():void 0});module.exports=empty;

},{"./internal/_curry1":107,"./internal/_isArguments":122,"./internal/_isArray":123,"./internal/_isObject":128,"./internal/_isString":131}],61:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),equals=require("./equals"),takeLast=require("./takeLast"),endsWith=_curry2(function(e,r){return equals(takeLast(e.length,r),e)});module.exports=endsWith;

},{"./equals":64,"./internal/_curry2":108,"./takeLast":280}],62:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),equals=require("./equals"),eqBy=_curry3(function(r,e,u){return equals(r(e),r(u))});module.exports=eqBy;

},{"./equals":64,"./internal/_curry3":109}],63:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),equals=require("./equals"),eqProps=_curry3(function(r,e,u){return equals(e[r],u[r])});module.exports=eqProps;

},{"./equals":64,"./internal/_curry3":109}],64:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_equals=require("./internal/_equals"),equals=_curry2(function(r,e){return _equals(r,e,[],[])});module.exports=equals;

},{"./internal/_curry2":108,"./internal/_equals":114}],65:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),evolve=_curry2(function r(e,o){var n,u,t,c={};for(u in o)t=typeof(n=e[u]),c[u]="function"===t?n(o[u]):n&&"object"===t?r(n,o[u]):o[u];return c});module.exports=evolve;

},{"./internal/_curry2":108}],66:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_filter=require("./internal/_filter"),_isObject=require("./internal/_isObject"),_reduce=require("./internal/_reduce"),_xfilter=require("./internal/_xfilter"),keys=require("./keys"),filter=_curry2(_dispatchable(["filter"],_xfilter,function(e,r){return _isObject(r)?_reduce(function(i,t){return e(r[t])&&(i[t]=r[t]),i},{},keys(r)):_filter(e,r)}));module.exports=filter;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_filter":115,"./internal/_isObject":128,"./internal/_reduce":140,"./internal/_xfilter":155,"./keys":177}],67:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xfind=require("./internal/_xfind"),find=_curry2(_dispatchable(["find"],_xfind,function(r,i){for(var e=0,n=i.length;e<n;){if(r(i[e]))return i[e];e+=1}}));module.exports=find;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xfind":156}],68:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xfindIndex=require("./internal/_xfindIndex"),findIndex=_curry2(_dispatchable([],_xfindIndex,function(r,e){for(var n=0,i=e.length;n<i;){if(r(e[n]))return n;n+=1}return-1}));module.exports=findIndex;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xfindIndex":157}],69:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xfindLast=require("./internal/_xfindLast"),findLast=_curry2(_dispatchable([],_xfindLast,function(r,a){for(var e=a.length-1;e>=0;){if(r(a[e]))return a[e];e-=1}}));module.exports=findLast;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xfindLast":158}],70:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xfindLastIndex=require("./internal/_xfindLastIndex"),findLastIndex=_curry2(_dispatchable([],_xfindLastIndex,function(r,e){for(var n=e.length-1;n>=0;){if(r(e[n]))return n;n-=1}return-1}));module.exports=findLastIndex;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xfindLastIndex":159}],71:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_makeFlat=require("./internal/_makeFlat"),flatten=_curry1(_makeFlat(!0));module.exports=flatten;

},{"./internal/_curry1":107,"./internal/_makeFlat":133}],72:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),curryN=require("./curryN"),flip=_curry1(function(r){return curryN(r.length,function(u,e){var c=Array.prototype.slice.call(arguments,0);return c[0]=e,c[1]=u,r.apply(this,c)})});module.exports=flip;

},{"./curryN":44,"./internal/_curry1":107}],73:[function(require,module,exports){
var _checkForMethod=require("./internal/_checkForMethod"),_curry2=require("./internal/_curry2"),forEach=_curry2(_checkForMethod("forEach",function(r,e){for(var c=e.length,o=0;o<c;)r(e[o]),o+=1;return e}));module.exports=forEach;

},{"./internal/_checkForMethod":99,"./internal/_curry2":108}],74:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),keys=require("./keys"),forEachObjIndexed=_curry2(function(r,e){for(var n=keys(e),u=0;u<n.length;){var a=n[u];r(e[a],a,e),u+=1}return e});module.exports=forEachObjIndexed;

},{"./internal/_curry2":108,"./keys":177}],75:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),fromPairs=_curry1(function(r){for(var e={},u=0;u<r.length;)e[r[u][0]]=r[u][1],u+=1;return e});module.exports=fromPairs;

},{"./internal/_curry1":107}],76:[function(require,module,exports){
var _checkForMethod=require("./internal/_checkForMethod"),_curry2=require("./internal/_curry2"),reduceBy=require("./reduceBy"),groupBy=_curry2(_checkForMethod("groupBy",reduceBy(function(r,e){return null==r&&(r=[]),r.push(e),r},null)));module.exports=groupBy;

},{"./internal/_checkForMethod":99,"./internal/_curry2":108,"./reduceBy":253}],77:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),groupWith=_curry2(function(r,u){for(var e=[],o=0,t=u.length;o<t;){for(var i=o+1;i<t&&r(u[i-1],u[i]);)i+=1;e.push(u.slice(o,i)),o=i}return e});module.exports=groupWith;

},{"./internal/_curry2":108}],78:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),gt=_curry2(function(r,u){return r>u});module.exports=gt;

},{"./internal/_curry2":108}],79:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),gte=_curry2(function(r,e){return r>=e});module.exports=gte;

},{"./internal/_curry2":108}],80:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_has=require("./internal/_has"),has=_curry2(_has);module.exports=has;

},{"./internal/_curry2":108,"./internal/_has":119}],81:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),hasIn=_curry2(function(r,n){return r in n});module.exports=hasIn;

},{"./internal/_curry2":108}],82:[function(require,module,exports){
var nth=require("./nth"),head=nth(0);module.exports=head;

},{"./nth":218}],83:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),identical=_curry2(function(r,e){return r===e?0!==r||1/r==1/e:r!=r&&e!=e});module.exports=identical;

},{"./internal/_curry2":108}],84:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_identity=require("./internal/_identity"),identity=_curry1(_identity);module.exports=identity;

},{"./internal/_curry1":107,"./internal/_identity":120}],85:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),curryN=require("./curryN"),ifElse=_curry3(function(r,e,t){return curryN(Math.max(r.length,e.length,t.length),function(){return r.apply(this,arguments)?e.apply(this,arguments):t.apply(this,arguments)})});module.exports=ifElse;

},{"./curryN":44,"./internal/_curry3":109}],86:[function(require,module,exports){
var add=require("./add"),inc=add(1);module.exports=inc;

},{"./add":6}],87:[function(require,module,exports){
module.exports={},module.exports.F=require("./F"),module.exports.T=require("./T"),module.exports.__=require("./__"),module.exports.add=require("./add"),module.exports.addIndex=require("./addIndex"),module.exports.adjust=require("./adjust"),module.exports.all=require("./all"),module.exports.allPass=require("./allPass"),module.exports.always=require("./always"),module.exports.and=require("./and"),module.exports.any=require("./any"),module.exports.anyPass=require("./anyPass"),module.exports.ap=require("./ap"),module.exports.aperture=require("./aperture"),module.exports.append=require("./append"),module.exports.apply=require("./apply"),module.exports.applySpec=require("./applySpec"),module.exports.applyTo=require("./applyTo"),module.exports.ascend=require("./ascend"),module.exports.assoc=require("./assoc"),module.exports.assocPath=require("./assocPath"),module.exports.binary=require("./binary"),module.exports.bind=require("./bind"),module.exports.both=require("./both"),module.exports.call=require("./call"),module.exports.chain=require("./chain"),module.exports.clamp=require("./clamp"),module.exports.clone=require("./clone"),module.exports.comparator=require("./comparator"),module.exports.complement=require("./complement"),module.exports.compose=require("./compose"),module.exports.composeK=require("./composeK"),module.exports.composeP=require("./composeP"),module.exports.concat=require("./concat"),module.exports.cond=require("./cond"),module.exports.construct=require("./construct"),module.exports.constructN=require("./constructN"),module.exports.contains=require("./contains"),module.exports.converge=require("./converge"),module.exports.countBy=require("./countBy"),module.exports.curry=require("./curry"),module.exports.curryN=require("./curryN"),module.exports.dec=require("./dec"),module.exports.defaultTo=require("./defaultTo"),module.exports.descend=require("./descend"),module.exports.difference=require("./difference"),module.exports.differenceWith=require("./differenceWith"),module.exports.dissoc=require("./dissoc"),module.exports.dissocPath=require("./dissocPath"),module.exports.divide=require("./divide"),module.exports.drop=require("./drop"),module.exports.dropLast=require("./dropLast"),module.exports.dropLastWhile=require("./dropLastWhile"),module.exports.dropRepeats=require("./dropRepeats"),module.exports.dropRepeatsWith=require("./dropRepeatsWith"),module.exports.dropWhile=require("./dropWhile"),module.exports.either=require("./either"),module.exports.empty=require("./empty"),module.exports.endsWith=require("./endsWith"),module.exports.eqBy=require("./eqBy"),module.exports.eqProps=require("./eqProps"),module.exports.equals=require("./equals"),module.exports.evolve=require("./evolve"),module.exports.filter=require("./filter"),module.exports.find=require("./find"),module.exports.findIndex=require("./findIndex"),module.exports.findLast=require("./findLast"),module.exports.findLastIndex=require("./findLastIndex"),module.exports.flatten=require("./flatten"),module.exports.flip=require("./flip"),module.exports.forEach=require("./forEach"),module.exports.forEachObjIndexed=require("./forEachObjIndexed"),module.exports.fromPairs=require("./fromPairs"),module.exports.groupBy=require("./groupBy"),module.exports.groupWith=require("./groupWith"),module.exports.gt=require("./gt"),module.exports.gte=require("./gte"),module.exports.has=require("./has"),module.exports.hasIn=require("./hasIn"),module.exports.head=require("./head"),module.exports.identical=require("./identical"),module.exports.identity=require("./identity"),module.exports.ifElse=require("./ifElse"),module.exports.inc=require("./inc"),module.exports.indexBy=require("./indexBy"),module.exports.indexOf=require("./indexOf"),module.exports.init=require("./init"),module.exports.innerJoin=require("./innerJoin"),module.exports.insert=require("./insert"),module.exports.insertAll=require("./insertAll"),module.exports.intersection=require("./intersection"),module.exports.intersperse=require("./intersperse"),module.exports.into=require("./into"),module.exports.invert=require("./invert"),module.exports.invertObj=require("./invertObj"),module.exports.invoker=require("./invoker"),module.exports.is=require("./is"),module.exports.isEmpty=require("./isEmpty"),module.exports.isNil=require("./isNil"),module.exports.join=require("./join"),module.exports.juxt=require("./juxt"),module.exports.keys=require("./keys"),module.exports.keysIn=require("./keysIn"),module.exports.last=require("./last"),module.exports.lastIndexOf=require("./lastIndexOf"),module.exports.length=require("./length"),module.exports.lens=require("./lens"),module.exports.lensIndex=require("./lensIndex"),module.exports.lensPath=require("./lensPath"),module.exports.lensProp=require("./lensProp"),module.exports.lift=require("./lift"),module.exports.liftN=require("./liftN"),module.exports.lt=require("./lt"),module.exports.lte=require("./lte"),module.exports.map=require("./map"),module.exports.mapAccum=require("./mapAccum"),module.exports.mapAccumRight=require("./mapAccumRight"),module.exports.mapObjIndexed=require("./mapObjIndexed"),module.exports.match=require("./match"),module.exports.mathMod=require("./mathMod"),module.exports.max=require("./max"),module.exports.maxBy=require("./maxBy"),module.exports.mean=require("./mean"),module.exports.median=require("./median"),module.exports.memoize=require("./memoize"),module.exports.memoizeWith=require("./memoizeWith"),module.exports.merge=require("./merge"),module.exports.mergeAll=require("./mergeAll"),module.exports.mergeDeepLeft=require("./mergeDeepLeft"),module.exports.mergeDeepRight=require("./mergeDeepRight"),module.exports.mergeDeepWith=require("./mergeDeepWith"),module.exports.mergeDeepWithKey=require("./mergeDeepWithKey"),module.exports.mergeWith=require("./mergeWith"),module.exports.mergeWithKey=require("./mergeWithKey"),module.exports.min=require("./min"),module.exports.minBy=require("./minBy"),module.exports.modulo=require("./modulo"),module.exports.multiply=require("./multiply"),module.exports.nAry=require("./nAry"),module.exports.negate=require("./negate"),module.exports.none=require("./none"),module.exports.not=require("./not"),module.exports.nth=require("./nth"),module.exports.nthArg=require("./nthArg"),module.exports.o=require("./o"),module.exports.objOf=require("./objOf"),module.exports.of=require("./of"),module.exports.omit=require("./omit"),module.exports.once=require("./once"),module.exports.or=require("./or"),module.exports.over=require("./over"),module.exports.pair=require("./pair"),module.exports.partial=require("./partial"),module.exports.partialRight=require("./partialRight"),module.exports.partition=require("./partition"),module.exports.path=require("./path"),module.exports.pathEq=require("./pathEq"),module.exports.pathOr=require("./pathOr"),module.exports.pathSatisfies=require("./pathSatisfies"),module.exports.pick=require("./pick"),module.exports.pickAll=require("./pickAll"),module.exports.pickBy=require("./pickBy"),module.exports.pipe=require("./pipe"),module.exports.pipeK=require("./pipeK"),module.exports.pipeP=require("./pipeP"),module.exports.pluck=require("./pluck"),module.exports.prepend=require("./prepend"),module.exports.product=require("./product"),module.exports.project=require("./project"),module.exports.prop=require("./prop"),module.exports.propEq=require("./propEq"),module.exports.propIs=require("./propIs"),module.exports.propOr=require("./propOr"),module.exports.propSatisfies=require("./propSatisfies"),module.exports.props=require("./props"),module.exports.range=require("./range"),module.exports.reduce=require("./reduce"),module.exports.reduceBy=require("./reduceBy"),module.exports.reduceRight=require("./reduceRight"),module.exports.reduceWhile=require("./reduceWhile"),module.exports.reduced=require("./reduced"),module.exports.reject=require("./reject"),module.exports.remove=require("./remove"),module.exports.repeat=require("./repeat"),module.exports.replace=require("./replace"),module.exports.reverse=require("./reverse"),module.exports.scan=require("./scan"),module.exports.sequence=require("./sequence"),module.exports.set=require("./set"),module.exports.slice=require("./slice"),module.exports.sort=require("./sort"),module.exports.sortBy=require("./sortBy"),module.exports.sortWith=require("./sortWith"),module.exports.split=require("./split"),module.exports.splitAt=require("./splitAt"),module.exports.splitEvery=require("./splitEvery"),module.exports.splitWhen=require("./splitWhen"),module.exports.startsWith=require("./startsWith"),module.exports.subtract=require("./subtract"),module.exports.sum=require("./sum"),module.exports.symmetricDifference=require("./symmetricDifference"),module.exports.symmetricDifferenceWith=require("./symmetricDifferenceWith"),module.exports.tail=require("./tail"),module.exports.take=require("./take"),module.exports.takeLast=require("./takeLast"),module.exports.takeLastWhile=require("./takeLastWhile"),module.exports.takeWhile=require("./takeWhile"),module.exports.tap=require("./tap"),module.exports.test=require("./test"),module.exports.times=require("./times"),module.exports.toLower=require("./toLower"),module.exports.toPairs=require("./toPairs"),module.exports.toPairsIn=require("./toPairsIn"),module.exports.toString=require("./toString"),module.exports.toUpper=require("./toUpper"),module.exports.transduce=require("./transduce"),module.exports.transpose=require("./transpose"),module.exports.traverse=require("./traverse"),module.exports.trim=require("./trim"),module.exports.tryCatch=require("./tryCatch"),module.exports.type=require("./type"),module.exports.unapply=require("./unapply"),module.exports.unary=require("./unary"),module.exports.uncurryN=require("./uncurryN"),module.exports.unfold=require("./unfold"),module.exports.union=require("./union"),module.exports.unionWith=require("./unionWith"),module.exports.uniq=require("./uniq"),module.exports.uniqBy=require("./uniqBy"),module.exports.uniqWith=require("./uniqWith"),module.exports.unless=require("./unless"),module.exports.unnest=require("./unnest"),module.exports.until=require("./until"),module.exports.update=require("./update"),module.exports.useWith=require("./useWith"),module.exports.values=require("./values"),module.exports.valuesIn=require("./valuesIn"),module.exports.view=require("./view"),module.exports.when=require("./when"),module.exports.where=require("./where"),module.exports.whereEq=require("./whereEq"),module.exports.without=require("./without"),module.exports.xprod=require("./xprod"),module.exports.zip=require("./zip"),module.exports.zipObj=require("./zipObj"),module.exports.zipWith=require("./zipWith");

},{"./F":3,"./T":4,"./__":5,"./add":6,"./addIndex":7,"./adjust":8,"./all":9,"./allPass":10,"./always":11,"./and":12,"./any":13,"./anyPass":14,"./ap":15,"./aperture":16,"./append":17,"./apply":18,"./applySpec":19,"./applyTo":20,"./ascend":21,"./assoc":22,"./assocPath":23,"./binary":24,"./bind":25,"./both":26,"./call":27,"./chain":28,"./clamp":29,"./clone":30,"./comparator":31,"./complement":32,"./compose":33,"./composeK":34,"./composeP":35,"./concat":36,"./cond":37,"./construct":38,"./constructN":39,"./contains":40,"./converge":41,"./countBy":42,"./curry":43,"./curryN":44,"./dec":45,"./defaultTo":46,"./descend":47,"./difference":48,"./differenceWith":49,"./dissoc":50,"./dissocPath":51,"./divide":52,"./drop":53,"./dropLast":54,"./dropLastWhile":55,"./dropRepeats":56,"./dropRepeatsWith":57,"./dropWhile":58,"./either":59,"./empty":60,"./endsWith":61,"./eqBy":62,"./eqProps":63,"./equals":64,"./evolve":65,"./filter":66,"./find":67,"./findIndex":68,"./findLast":69,"./findLastIndex":70,"./flatten":71,"./flip":72,"./forEach":73,"./forEachObjIndexed":74,"./fromPairs":75,"./groupBy":76,"./groupWith":77,"./gt":78,"./gte":79,"./has":80,"./hasIn":81,"./head":82,"./identical":83,"./identity":84,"./ifElse":85,"./inc":86,"./indexBy":88,"./indexOf":89,"./init":90,"./innerJoin":91,"./insert":92,"./insertAll":93,"./intersection":166,"./intersperse":167,"./into":168,"./invert":169,"./invertObj":170,"./invoker":171,"./is":172,"./isEmpty":173,"./isNil":174,"./join":175,"./juxt":176,"./keys":177,"./keysIn":178,"./last":179,"./lastIndexOf":180,"./length":181,"./lens":182,"./lensIndex":183,"./lensPath":184,"./lensProp":185,"./lift":186,"./liftN":187,"./lt":188,"./lte":189,"./map":190,"./mapAccum":191,"./mapAccumRight":192,"./mapObjIndexed":193,"./match":194,"./mathMod":195,"./max":196,"./maxBy":197,"./mean":198,"./median":199,"./memoize":200,"./memoizeWith":201,"./merge":202,"./mergeAll":203,"./mergeDeepLeft":204,"./mergeDeepRight":205,"./mergeDeepWith":206,"./mergeDeepWithKey":207,"./mergeWith":208,"./mergeWithKey":209,"./min":210,"./minBy":211,"./modulo":212,"./multiply":213,"./nAry":214,"./negate":215,"./none":216,"./not":217,"./nth":218,"./nthArg":219,"./o":220,"./objOf":221,"./of":222,"./omit":223,"./once":224,"./or":225,"./over":226,"./pair":227,"./partial":228,"./partialRight":229,"./partition":230,"./path":231,"./pathEq":232,"./pathOr":233,"./pathSatisfies":234,"./pick":235,"./pickAll":236,"./pickBy":237,"./pipe":238,"./pipeK":239,"./pipeP":240,"./pluck":241,"./prepend":242,"./product":243,"./project":244,"./prop":245,"./propEq":246,"./propIs":247,"./propOr":248,"./propSatisfies":249,"./props":250,"./range":251,"./reduce":252,"./reduceBy":253,"./reduceRight":254,"./reduceWhile":255,"./reduced":256,"./reject":257,"./remove":258,"./repeat":259,"./replace":260,"./reverse":261,"./scan":262,"./sequence":263,"./set":264,"./slice":265,"./sort":266,"./sortBy":267,"./sortWith":268,"./split":269,"./splitAt":270,"./splitEvery":271,"./splitWhen":272,"./startsWith":273,"./subtract":274,"./sum":275,"./symmetricDifference":276,"./symmetricDifferenceWith":277,"./tail":278,"./take":279,"./takeLast":280,"./takeLastWhile":281,"./takeWhile":282,"./tap":283,"./test":284,"./times":285,"./toLower":286,"./toPairs":287,"./toPairsIn":288,"./toString":289,"./toUpper":290,"./transduce":291,"./transpose":292,"./traverse":293,"./trim":294,"./tryCatch":295,"./type":296,"./unapply":297,"./unary":298,"./uncurryN":299,"./unfold":300,"./union":301,"./unionWith":302,"./uniq":303,"./uniqBy":304,"./uniqWith":305,"./unless":306,"./unnest":307,"./until":308,"./update":309,"./useWith":310,"./values":311,"./valuesIn":312,"./view":313,"./when":314,"./where":315,"./whereEq":316,"./without":317,"./xprod":318,"./zip":319,"./zipObj":320,"./zipWith":321}],88:[function(require,module,exports){
var reduceBy=require("./reduceBy"),indexBy=reduceBy(function(e,r){return r},null);module.exports=indexBy;

},{"./reduceBy":253}],89:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_indexOf=require("./internal/_indexOf"),_isArray=require("./internal/_isArray"),indexOf=_curry2(function(r,e){return"function"!=typeof e.indexOf||_isArray(e)?_indexOf(e,r,0):e.indexOf(r)});module.exports=indexOf;

},{"./internal/_curry2":108,"./internal/_indexOf":121,"./internal/_isArray":123}],90:[function(require,module,exports){
var slice=require("./slice"),init=slice(0,-1);module.exports=init;

},{"./slice":265}],91:[function(require,module,exports){
var _containsWith=require("./internal/_containsWith"),_curry3=require("./internal/_curry3"),_filter=require("./internal/_filter"),innerJoin=_curry3(function(r,n,i){return _filter(function(n){return _containsWith(r,n,i)},n)});module.exports=innerJoin;

},{"./internal/_containsWith":105,"./internal/_curry3":109,"./internal/_filter":115}],92:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),insert=_curry3(function(r,e,t){r=r<t.length&&r>=0?r:t.length;var n=Array.prototype.slice.call(t,0);return n.splice(r,0,e),n});module.exports=insert;

},{"./internal/_curry3":109}],93:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),insertAll=_curry3(function(r,e,l){return r=r<l.length&&r>=0?r:l.length,[].concat(Array.prototype.slice.call(l,0,r),e,Array.prototype.slice.call(l,r))});module.exports=insertAll;

},{"./internal/_curry3":109}],94:[function(require,module,exports){
var _contains=require("./_contains"),_Set=function(){function t(){this._nativeSet="function"==typeof Set?new Set:null,this._items={}}return t.prototype.add=function(t){return!hasOrAdd(t,!0,this)},t.prototype.has=function(t){return hasOrAdd(t,!1,this)},t}();function hasOrAdd(t,e,i){var n,s=typeof t;switch(s){case"string":case"number":return 0===t&&1/t==-1/0?!!i._items["-0"]||(e&&(i._items["-0"]=!0),!1):null!==i._nativeSet?e?(n=i._nativeSet.size,i._nativeSet.add(t),i._nativeSet.size===n):i._nativeSet.has(t):s in i._items?t in i._items[s]||(e&&(i._items[s][t]=!0),!1):(e&&(i._items[s]={},i._items[s][t]=!0),!1);case"boolean":if(s in i._items){var _=t?1:0;return!!i._items[s][_]||(e&&(i._items[s][_]=!0),!1)}return e&&(i._items[s]=t?[!1,!0]:[!0,!1]),!1;case"function":return null!==i._nativeSet?e?(n=i._nativeSet.size,i._nativeSet.add(t),i._nativeSet.size===n):i._nativeSet.has(t):s in i._items?!!_contains(t,i._items[s])||(e&&i._items[s].push(t),!1):(e&&(i._items[s]=[t]),!1);case"undefined":return!!i._items[s]||(e&&(i._items[s]=!0),!1);case"object":if(null===t)return!!i._items.null||(e&&(i._items.null=!0),!1);default:return(s=Object.prototype.toString.call(t))in i._items?!!_contains(t,i._items[s])||(e&&i._items[s].push(t),!1):(e&&(i._items[s]=[t]),!1)}}module.exports=_Set;

},{"./_contains":104}],95:[function(require,module,exports){
function _aperture(r,e){for(var t=0,a=e.length-(r-1),o=new Array(a>=0?a:0);t<a;)o[t]=Array.prototype.slice.call(e,t,t+r),t+=1;return o}module.exports=_aperture;

},{}],96:[function(require,module,exports){
function _arity(t,r){switch(t){case 0:return function(){return r.apply(this,arguments)};case 1:return function(t){return r.apply(this,arguments)};case 2:return function(t,n){return r.apply(this,arguments)};case 3:return function(t,n,e){return r.apply(this,arguments)};case 4:return function(t,n,e,u){return r.apply(this,arguments)};case 5:return function(t,n,e,u,a){return r.apply(this,arguments)};case 6:return function(t,n,e,u,a,i){return r.apply(this,arguments)};case 7:return function(t,n,e,u,a,i,s){return r.apply(this,arguments)};case 8:return function(t,n,e,u,a,i,s,c){return r.apply(this,arguments)};case 9:return function(t,n,e,u,a,i,s,c,p){return r.apply(this,arguments)};case 10:return function(t,n,e,u,a,i,s,c,p,o){return r.apply(this,arguments)};default:throw new Error("First argument to _arity must be a non-negative integer no greater than ten")}}module.exports=_arity;

},{}],97:[function(require,module,exports){
function _arrayFromIterator(r){for(var o,a=[];!(o=r.next()).done;)a.push(o.value);return a}module.exports=_arrayFromIterator;

},{}],98:[function(require,module,exports){
var _objectAssign=require("./_objectAssign");module.exports="function"==typeof Object.assign?Object.assign:_objectAssign;

},{"./_objectAssign":135}],99:[function(require,module,exports){
var _isArray=require("./_isArray");function _checkForMethod(r,e){return function(){var t=arguments.length;if(0===t)return e();var o=arguments[t-1];return _isArray(o)||"function"!=typeof o[r]?e.apply(this,arguments):o[r].apply(o,Array.prototype.slice.call(arguments,0,t-1))}}module.exports=_checkForMethod;

},{"./_isArray":123}],100:[function(require,module,exports){
var _cloneRegExp=require("./_cloneRegExp"),type=require("../type");function _clone(e,r,n,t){var c=function(c){for(var u=r.length,a=0;a<u;){if(e===r[a])return n[a];a+=1}for(var o in r[a+1]=e,n[a+1]=c,e)c[o]=t?_clone(e[o],r,n,!0):e[o];return c};switch(type(e)){case"Object":return c({});case"Array":return c([]);case"Date":return new Date(e.valueOf());case"RegExp":return _cloneRegExp(e);default:return e}}module.exports=_clone;

},{"../type":296,"./_cloneRegExp":101}],101:[function(require,module,exports){
function _cloneRegExp(e){return new RegExp(e.source,(e.global?"g":"")+(e.ignoreCase?"i":"")+(e.multiline?"m":"")+(e.sticky?"y":"")+(e.unicode?"u":""))}module.exports=_cloneRegExp;

},{}],102:[function(require,module,exports){
function _complement(e){return function(){return!e.apply(this,arguments)}}module.exports=_complement;

},{}],103:[function(require,module,exports){
function _concat(n,t){var e;t=t||[];var o=(n=n||[]).length,r=t.length,c=[];for(e=0;e<o;)c[c.length]=n[e],e+=1;for(e=0;e<r;)c[c.length]=t[e],e+=1;return c}module.exports=_concat;

},{}],104:[function(require,module,exports){
var _indexOf=require("./_indexOf");function _contains(n,e){return _indexOf(e,n,0)>=0}module.exports=_contains;

},{"./_indexOf":121}],105:[function(require,module,exports){
function _containsWith(n,t,r){for(var i=0,o=r.length;i<o;){if(n(t,r[i]))return!0;i+=1}return!1}module.exports=_containsWith;

},{}],106:[function(require,module,exports){
var _arity=require("./_arity"),_curry2=require("./_curry2");function _createPartialApplicator(r){return _curry2(function(t,a){return _arity(Math.max(0,t.length-a.length),function(){return t.apply(this,r(a,arguments))})})}module.exports=_createPartialApplicator;

},{"./_arity":96,"./_curry2":108}],107:[function(require,module,exports){
var _isPlaceholder=require("./_isPlaceholder");function _curry1(r){return function e(l){return 0===arguments.length||_isPlaceholder(l)?e:r.apply(this,arguments)}}module.exports=_curry1;

},{"./_isPlaceholder":129}],108:[function(require,module,exports){
var _curry1=require("./_curry1"),_isPlaceholder=require("./_isPlaceholder");function _curry2(r){return function e(u,c){switch(arguments.length){case 0:return e;case 1:return _isPlaceholder(u)?e:_curry1(function(e){return r(u,e)});default:return _isPlaceholder(u)&&_isPlaceholder(c)?e:_isPlaceholder(u)?_curry1(function(e){return r(e,c)}):_isPlaceholder(c)?_curry1(function(e){return r(u,e)}):r(u,c)}}}module.exports=_curry2;

},{"./_curry1":107,"./_isPlaceholder":129}],109:[function(require,module,exports){
var _curry1=require("./_curry1"),_curry2=require("./_curry2"),_isPlaceholder=require("./_isPlaceholder");function _curry3(r){return function e(c,u,l){switch(arguments.length){case 0:return e;case 1:return _isPlaceholder(c)?e:_curry2(function(e,u){return r(c,e,u)});case 2:return _isPlaceholder(c)&&_isPlaceholder(u)?e:_isPlaceholder(c)?_curry2(function(e,c){return r(e,u,c)}):_isPlaceholder(u)?_curry2(function(e,u){return r(c,e,u)}):_curry1(function(e){return r(c,u,e)});default:return _isPlaceholder(c)&&_isPlaceholder(u)&&_isPlaceholder(l)?e:_isPlaceholder(c)&&_isPlaceholder(u)?_curry2(function(e,c){return r(e,c,l)}):_isPlaceholder(c)&&_isPlaceholder(l)?_curry2(function(e,c){return r(e,u,c)}):_isPlaceholder(u)&&_isPlaceholder(l)?_curry2(function(e,u){return r(c,e,u)}):_isPlaceholder(c)?_curry1(function(e){return r(e,u,l)}):_isPlaceholder(u)?_curry1(function(e){return r(c,e,l)}):_isPlaceholder(l)?_curry1(function(e){return r(c,u,e)}):r(c,u,l)}}}module.exports=_curry3;

},{"./_curry1":107,"./_curry2":108,"./_isPlaceholder":129}],110:[function(require,module,exports){
var _arity=require("./_arity"),_isPlaceholder=require("./_isPlaceholder");function _curryN(r,e,l){return function(){for(var t=[],i=0,a=r,n=0;n<e.length||i<arguments.length;){var u;n<e.length&&(!_isPlaceholder(e[n])||i>=arguments.length)?u=e[n]:(u=arguments[i],i+=1),t[n]=u,_isPlaceholder(u)||(a-=1),n+=1}return a<=0?l.apply(this,t):_arity(a,_curryN(r,t,l))}}module.exports=_curryN;

},{"./_arity":96,"./_isPlaceholder":129}],111:[function(require,module,exports){
var _isArray=require("./_isArray"),_isTransformer=require("./_isTransformer");function _dispatchable(r,e,i){return function(){if(0===arguments.length)return i();var a=Array.prototype.slice.call(arguments,0),n=a.pop();if(!_isArray(n)){for(var t=0;t<r.length;){if("function"==typeof n[r[t]])return n[r[t]].apply(n,a);t+=1}if(_isTransformer(n))return e.apply(null,a)(n)}return i.apply(this,arguments)}}module.exports=_dispatchable;

},{"./_isArray":123,"./_isTransformer":132}],112:[function(require,module,exports){
var take=require("../take");function dropLast(e,t){return take(e<t.length?t.length-e:0,t)}module.exports=dropLast;

},{"../take":279}],113:[function(require,module,exports){
var slice=require("../slice");function dropLastWhile(e,r){for(var i=r.length-1;i>=0&&e(r[i]);)i-=1;return slice(0,i+1,r)}module.exports=dropLastWhile;

},{"../slice":265}],114:[function(require,module,exports){
var _arrayFromIterator=require("./_arrayFromIterator"),_containsWith=require("./_containsWith"),_functionName=require("./_functionName"),_has=require("./_has"),identical=require("../identical"),keys=require("../keys"),type=require("../type");function _uniqContentEquals(e,a,r,t){var n=_arrayFromIterator(e),s=_arrayFromIterator(a);function u(e,a){return _equals(e,a,r.slice(),t.slice())}return!_containsWith(function(e,a){return!_containsWith(u,a,e)},s,n)}function _equals(e,a,r,t){if(identical(e,a))return!0;var n=type(e);if(n!==type(a))return!1;if(null==e||null==a)return!1;if("function"==typeof e["fantasy-land/equals"]||"function"==typeof a["fantasy-land/equals"])return"function"==typeof e["fantasy-land/equals"]&&e["fantasy-land/equals"](a)&&"function"==typeof a["fantasy-land/equals"]&&a["fantasy-land/equals"](e);if("function"==typeof e.equals||"function"==typeof a.equals)return"function"==typeof e.equals&&e.equals(a)&&"function"==typeof a.equals&&a.equals(e);switch(n){case"Arguments":case"Array":case"Object":if("function"==typeof e.constructor&&"Promise"===_functionName(e.constructor))return e===a;break;case"Boolean":case"Number":case"String":if(typeof e!=typeof a||!identical(e.valueOf(),a.valueOf()))return!1;break;case"Date":if(!identical(e.valueOf(),a.valueOf()))return!1;break;case"Error":return e.name===a.name&&e.message===a.message;case"RegExp":if(e.source!==a.source||e.global!==a.global||e.ignoreCase!==a.ignoreCase||e.multiline!==a.multiline||e.sticky!==a.sticky||e.unicode!==a.unicode)return!1}for(var s=r.length-1;s>=0;){if(r[s]===e)return t[s]===a;s-=1}switch(n){case"Map":return e.size===a.size&&_uniqContentEquals(e.entries(),a.entries(),r.concat([e]),t.concat([a]));case"Set":return e.size===a.size&&_uniqContentEquals(e.values(),a.values(),r.concat([e]),t.concat([a]));case"Arguments":case"Array":case"Object":case"Boolean":case"Number":case"String":case"Date":case"Error":case"RegExp":case"Int8Array":case"Uint8Array":case"Uint8ClampedArray":case"Int16Array":case"Uint16Array":case"Int32Array":case"Uint32Array":case"Float32Array":case"Float64Array":case"ArrayBuffer":break;default:return!1}var u=keys(e);if(u.length!==keys(a).length)return!1;var c=r.concat([e]),i=t.concat([a]);for(s=u.length-1;s>=0;){var o=u[s];if(!_has(o,a)||!_equals(a[o],e[o],c,i))return!1;s-=1}return!0}module.exports=_equals;

},{"../identical":83,"../keys":177,"../type":296,"./_arrayFromIterator":97,"./_containsWith":105,"./_functionName":118,"./_has":119}],115:[function(require,module,exports){
function _filter(e,r){for(var t=0,l=r.length,n=[];t<l;)e(r[t])&&(n[n.length]=r[t]),t+=1;return n}module.exports=_filter;

},{}],116:[function(require,module,exports){
var _forceReduced=require("./_forceReduced"),_isArrayLike=require("./_isArrayLike"),_reduce=require("./_reduce"),_xfBase=require("./_xfBase"),preservingReduced=function(e){return{"@@transducer/init":_xfBase.init,"@@transducer/result":function(r){return e["@@transducer/result"](r)},"@@transducer/step":function(r,u){var t=e["@@transducer/step"](r,u);return t["@@transducer/reduced"]?_forceReduced(t):t}}},_flatCat=function(e){var r=preservingReduced(e);return{"@@transducer/init":_xfBase.init,"@@transducer/result":function(e){return r["@@transducer/result"](e)},"@@transducer/step":function(e,u){return _isArrayLike(u)?_reduce(r,e,u):_reduce(r,e,[u])}}};module.exports=_flatCat;

},{"./_forceReduced":117,"./_isArrayLike":124,"./_reduce":140,"./_xfBase":154}],117:[function(require,module,exports){
function _forceReduced(e){return{"@@transducer/value":e,"@@transducer/reduced":!0}}module.exports=_forceReduced;

},{}],118:[function(require,module,exports){
function _functionName(n){var t=String(n).match(/^function (\w*)/);return null==t?"":t[1]}module.exports=_functionName;

},{}],119:[function(require,module,exports){
function _has(t,e){return Object.prototype.hasOwnProperty.call(e,t)}module.exports=_has;

},{}],120:[function(require,module,exports){
function _identity(t){return t}module.exports=_identity;

},{}],121:[function(require,module,exports){
var equals=require("../equals");function _indexOf(e,n,r){var f,t;if("function"==typeof e.indexOf)switch(typeof n){case"number":if(0===n){for(f=1/n;r<e.length;){if(0===(t=e[r])&&1/t===f)return r;r+=1}return-1}if(n!=n){for(;r<e.length;){if("number"==typeof(t=e[r])&&t!=t)return r;r+=1}return-1}return e.indexOf(n,r);case"string":case"boolean":case"function":case"undefined":return e.indexOf(n,r);case"object":if(null===n)return e.indexOf(n,r)}for(;r<e.length;){if(equals(e[r],n))return r;r+=1}return-1}module.exports=_indexOf;

},{"../equals":64}],122:[function(require,module,exports){
var _has=require("./_has"),toString=Object.prototype.toString,_isArguments=function(){return"[object Arguments]"===toString.call(arguments)?function(t){return"[object Arguments]"===toString.call(t)}:function(t){return _has("callee",t)}};module.exports=_isArguments;

},{"./_has":119}],123:[function(require,module,exports){
module.exports=Array.isArray||function(r){return null!=r&&r.length>=0&&"[object Array]"===Object.prototype.toString.call(r)};

},{}],124:[function(require,module,exports){
var _curry1=require("./_curry1"),_isArray=require("./_isArray"),_isString=require("./_isString"),_isArrayLike=_curry1(function(r){return!!_isArray(r)||!!r&&("object"==typeof r&&(!_isString(r)&&(1===r.nodeType?!!r.length:0===r.length||r.length>0&&(r.hasOwnProperty(0)&&r.hasOwnProperty(r.length-1)))))});module.exports=_isArrayLike;

},{"./_curry1":107,"./_isArray":123,"./_isString":131}],125:[function(require,module,exports){
function _isFunction(t){return"[object Function]"===Object.prototype.toString.call(t)}module.exports=_isFunction;

},{}],126:[function(require,module,exports){
module.exports=Number.isInteger||function(e){return e<<0===e};

},{}],127:[function(require,module,exports){
function _isNumber(e){return"[object Number]"===Object.prototype.toString.call(e)}module.exports=_isNumber;

},{}],128:[function(require,module,exports){
function _isObject(t){return"[object Object]"===Object.prototype.toString.call(t)}module.exports=_isObject;

},{}],129:[function(require,module,exports){
function _isPlaceholder(e){return null!=e&&"object"==typeof e&&!0===e["@@functional/placeholder"]}module.exports=_isPlaceholder;

},{}],130:[function(require,module,exports){
function _isRegExp(e){return"[object RegExp]"===Object.prototype.toString.call(e)}module.exports=_isRegExp;

},{}],131:[function(require,module,exports){
function _isString(t){return"[object String]"===Object.prototype.toString.call(t)}module.exports=_isString;

},{}],132:[function(require,module,exports){
function _isTransformer(r){return"function"==typeof r["@@transducer/step"]}module.exports=_isTransformer;

},{}],133:[function(require,module,exports){
var _isArrayLike=require("./_isArrayLike");function _makeFlat(e){return function r(t){for(var i,n,a,l=[],o=0,u=t.length;o<u;){if(_isArrayLike(t[o]))for(a=0,n=(i=e?r(t[o]):t[o]).length;a<n;)l[l.length]=i[a],a+=1;else l[l.length]=t[o];o+=1}return l}}module.exports=_makeFlat;

},{"./_isArrayLike":124}],134:[function(require,module,exports){
function _map(r,a){for(var e=0,n=a.length,o=Array(n);e<n;)o[e]=r(a[e]),e+=1;return o}module.exports=_map;

},{}],135:[function(require,module,exports){
var _has=require("./_has");function _objectAssign(r){if(null==r)throw new TypeError("Cannot convert undefined or null to object");for(var n=Object(r),e=1,o=arguments.length;e<o;){var t=arguments[e];if(null!=t)for(var a in t)_has(a,t)&&(n[a]=t[a]);e+=1}return n}module.exports=_objectAssign;

},{"./_has":119}],136:[function(require,module,exports){
function _of(o){return[o]}module.exports=_of;

},{}],137:[function(require,module,exports){
function _pipe(p,t){return function(){return t.call(this,p.apply(this,arguments))}}module.exports=_pipe;

},{}],138:[function(require,module,exports){
function _pipeP(n,t){return function(){var e=this;return n.apply(e,arguments).then(function(n){return t.call(e,n)})}}module.exports=_pipeP;

},{}],139:[function(require,module,exports){
function _quote(e){return'"'+e.replace(/\\/g,"\\\\").replace(/[\b]/g,"\\b").replace(/\f/g,"\\f").replace(/\n/g,"\\n").replace(/\r/g,"\\r").replace(/\t/g,"\\t").replace(/\v/g,"\\v").replace(/\0/g,"\\0").replace(/"/g,'\\"')+'"'}module.exports=_quote;

},{}],140:[function(require,module,exports){
var _isArrayLike=require("./_isArrayLike"),_xwrap=require("./_xwrap"),bind=require("../bind");function _arrayReduce(e,r,t){for(var u=0,n=t.length;u<n;){if((r=e["@@transducer/step"](r,t[u]))&&r["@@transducer/reduced"]){r=r["@@transducer/value"];break}u+=1}return e["@@transducer/result"](r)}function _iterableReduce(e,r,t){for(var u=t.next();!u.done;){if((r=e["@@transducer/step"](r,u.value))&&r["@@transducer/reduced"]){r=r["@@transducer/value"];break}u=t.next()}return e["@@transducer/result"](r)}function _methodReduce(e,r,t,u){return e["@@transducer/result"](t[u](bind(e["@@transducer/step"],e),r))}var symIterator="undefined"!=typeof Symbol?Symbol.iterator:"@@iterator";function _reduce(e,r,t){if("function"==typeof e&&(e=_xwrap(e)),_isArrayLike(t))return _arrayReduce(e,r,t);if("function"==typeof t["fantasy-land/reduce"])return _methodReduce(e,r,t,"fantasy-land/reduce");if(null!=t[symIterator])return _iterableReduce(e,r,t[symIterator]());if("function"==typeof t.next)return _iterableReduce(e,r,t);if("function"==typeof t.reduce)return _methodReduce(e,r,t,"reduce");throw new TypeError("reduce: list must be array or iterable")}module.exports=_reduce;

},{"../bind":25,"./_isArrayLike":124,"./_xwrap":165}],141:[function(require,module,exports){
function _reduced(e){return e&&e["@@transducer/reduced"]?e:{"@@transducer/value":e,"@@transducer/reduced":!0}}module.exports=_reduced;

},{}],142:[function(require,module,exports){
var _assign=require("./_assign"),_identity=require("./_identity"),_isArrayLike=require("./_isArrayLike"),_isTransformer=require("./_isTransformer"),objOf=require("../objOf"),_stepCatArray={"@@transducer/init":Array,"@@transducer/step":function(r,t){return r.push(t),r},"@@transducer/result":_identity},_stepCatString={"@@transducer/init":String,"@@transducer/step":function(r,t){return r+t},"@@transducer/result":_identity},_stepCatObject={"@@transducer/init":Object,"@@transducer/step":function(r,t){return _assign(r,_isArrayLike(t)?objOf(t[0],t[1]):t)},"@@transducer/result":_identity};function _stepCat(r){if(_isTransformer(r))return r;if(_isArrayLike(r))return _stepCatArray;if("string"==typeof r)return _stepCatString;if("object"==typeof r)return _stepCatObject;throw new Error("Cannot create transformer for "+r)}module.exports=_stepCat;

},{"../objOf":221,"./_assign":98,"./_identity":120,"./_isArrayLike":124,"./_isTransformer":132}],143:[function(require,module,exports){
var pad=function(t){return(t<10?"0":"")+t},_toISOString="function"==typeof Date.prototype.toISOString?function(t){return t.toISOString()}:function(t){return t.getUTCFullYear()+"-"+pad(t.getUTCMonth()+1)+"-"+pad(t.getUTCDate())+"T"+pad(t.getUTCHours())+":"+pad(t.getUTCMinutes())+":"+pad(t.getUTCSeconds())+"."+(t.getUTCMilliseconds()/1e3).toFixed(3).slice(2,5)+"Z"};module.exports=_toISOString;

},{}],144:[function(require,module,exports){
var _contains=require("./_contains"),_map=require("./_map"),_quote=require("./_quote"),_toISOString=require("./_toISOString"),keys=require("../keys"),reject=require("../reject");function _toString(e,t){var r=function(r){var n=t.concat([e]);return _contains(r,n)?"<Circular>":_toString(r,n)},n=function(e,t){return _map(function(t){return _quote(t)+": "+r(e[t])},t.slice().sort())};switch(Object.prototype.toString.call(e)){case"[object Arguments]":return"(function() { return arguments; }("+_map(r,e).join(", ")+"))";case"[object Array]":return"["+_map(r,e).concat(n(e,reject(function(e){return/^\d+$/.test(e)},keys(e)))).join(", ")+"]";case"[object Boolean]":return"object"==typeof e?"new Boolean("+r(e.valueOf())+")":e.toString();case"[object Date]":return"new Date("+(isNaN(e.valueOf())?r(NaN):_quote(_toISOString(e)))+")";case"[object Null]":return"null";case"[object Number]":return"object"==typeof e?"new Number("+r(e.valueOf())+")":1/e==-1/0?"-0":e.toString(10);case"[object String]":return"object"==typeof e?"new String("+r(e.valueOf())+")":_quote(e);case"[object Undefined]":return"undefined";default:if("function"==typeof e.toString){var o=e.toString();if("[object Object]"!==o)return o}return"{"+n(e,keys(e)).join(", ")+"}"}}module.exports=_toString;

},{"../keys":177,"../reject":257,"./_contains":104,"./_map":134,"./_quote":139,"./_toISOString":143}],145:[function(require,module,exports){
var _curry2=require("./_curry2"),_reduced=require("./_reduced"),_xfBase=require("./_xfBase"),XAll=function(){function r(r,t){this.xf=t,this.f=r,this.all=!0}return r.prototype["@@transducer/init"]=_xfBase.init,r.prototype["@@transducer/result"]=function(r){return this.all&&(r=this.xf["@@transducer/step"](r,!0)),this.xf["@@transducer/result"](r)},r.prototype["@@transducer/step"]=function(r,t){return this.f(t)||(this.all=!1,r=_reduced(this.xf["@@transducer/step"](r,!1))),r},r}(),_xall=_curry2(function(r,t){return new XAll(r,t)});module.exports=_xall;

},{"./_curry2":108,"./_reduced":141,"./_xfBase":154}],146:[function(require,module,exports){
var _curry2=require("./_curry2"),_reduced=require("./_reduced"),_xfBase=require("./_xfBase"),XAny=function(){function r(r,t){this.xf=t,this.f=r,this.any=!1}return r.prototype["@@transducer/init"]=_xfBase.init,r.prototype["@@transducer/result"]=function(r){return this.any||(r=this.xf["@@transducer/step"](r,!1)),this.xf["@@transducer/result"](r)},r.prototype["@@transducer/step"]=function(r,t){return this.f(t)&&(this.any=!0,r=_reduced(this.xf["@@transducer/step"](r,!0))),r},r}(),_xany=_curry2(function(r,t){return new XAny(r,t)});module.exports=_xany;

},{"./_curry2":108,"./_reduced":141,"./_xfBase":154}],147:[function(require,module,exports){
var _concat=require("./_concat"),_curry2=require("./_curry2"),_xfBase=require("./_xfBase"),XAperture=function(){function t(t,r){this.xf=r,this.pos=0,this.full=!1,this.acc=new Array(t)}return t.prototype["@@transducer/init"]=_xfBase.init,t.prototype["@@transducer/result"]=function(t){return this.acc=null,this.xf["@@transducer/result"](t)},t.prototype["@@transducer/step"]=function(t,r){return this.store(r),this.full?this.xf["@@transducer/step"](t,this.getCopy()):t},t.prototype.store=function(t){this.acc[this.pos]=t,this.pos+=1,this.pos===this.acc.length&&(this.pos=0,this.full=!0)},t.prototype.getCopy=function(){return _concat(Array.prototype.slice.call(this.acc,this.pos),Array.prototype.slice.call(this.acc,0,this.pos))},t}(),_xaperture=_curry2(function(t,r){return new XAperture(t,r)});module.exports=_xaperture;

},{"./_concat":103,"./_curry2":108,"./_xfBase":154}],148:[function(require,module,exports){
var _curry2=require("./_curry2"),_flatCat=require("./_flatCat"),map=require("../map"),_xchain=_curry2(function(r,a){return map(r,_flatCat(a))});module.exports=_xchain;

},{"../map":190,"./_curry2":108,"./_flatCat":116}],149:[function(require,module,exports){
var _curry2=require("./_curry2"),_xfBase=require("./_xfBase"),XDrop=function(){function r(r,t){this.xf=t,this.n=r}return r.prototype["@@transducer/init"]=_xfBase.init,r.prototype["@@transducer/result"]=_xfBase.result,r.prototype["@@transducer/step"]=function(r,t){return this.n>0?(this.n-=1,r):this.xf["@@transducer/step"](r,t)},r}(),_xdrop=_curry2(function(r,t){return new XDrop(r,t)});module.exports=_xdrop;

},{"./_curry2":108,"./_xfBase":154}],150:[function(require,module,exports){
var _curry2=require("./_curry2"),_xfBase=require("./_xfBase"),XDropLast=function(){function t(t,r){this.xf=r,this.pos=0,this.full=!1,this.acc=new Array(t)}return t.prototype["@@transducer/init"]=_xfBase.init,t.prototype["@@transducer/result"]=function(t){return this.acc=null,this.xf["@@transducer/result"](t)},t.prototype["@@transducer/step"]=function(t,r){return this.full&&(t=this.xf["@@transducer/step"](t,this.acc[this.pos])),this.store(r),t},t.prototype.store=function(t){this.acc[this.pos]=t,this.pos+=1,this.pos===this.acc.length&&(this.pos=0,this.full=!0)},t}(),_xdropLast=_curry2(function(t,r){return new XDropLast(t,r)});module.exports=_xdropLast;

},{"./_curry2":108,"./_xfBase":154}],151:[function(require,module,exports){
var _curry2=require("./_curry2"),_reduce=require("./_reduce"),_xfBase=require("./_xfBase"),XDropLastWhile=function(){function t(t,r){this.f=t,this.retained=[],this.xf=r}return t.prototype["@@transducer/init"]=_xfBase.init,t.prototype["@@transducer/result"]=function(t){return this.retained=null,this.xf["@@transducer/result"](t)},t.prototype["@@transducer/step"]=function(t,r){return this.f(r)?this.retain(t,r):this.flush(t,r)},t.prototype.flush=function(t,r){return t=_reduce(this.xf["@@transducer/step"],t,this.retained),this.retained=[],this.xf["@@transducer/step"](t,r)},t.prototype.retain=function(t,r){return this.retained.push(r),t},t}(),_xdropLastWhile=_curry2(function(t,r){return new XDropLastWhile(t,r)});module.exports=_xdropLastWhile;

},{"./_curry2":108,"./_reduce":140,"./_xfBase":154}],152:[function(require,module,exports){
var _curry2=require("./_curry2"),_xfBase=require("./_xfBase"),XDropRepeatsWith=function(){function t(t,e){this.xf=e,this.pred=t,this.lastValue=void 0,this.seenFirstValue=!1}return t.prototype["@@transducer/init"]=_xfBase.init,t.prototype["@@transducer/result"]=_xfBase.result,t.prototype["@@transducer/step"]=function(t,e){var r=!1;return this.seenFirstValue?this.pred(this.lastValue,e)&&(r=!0):this.seenFirstValue=!0,this.lastValue=e,r?t:this.xf["@@transducer/step"](t,e)},t}(),_xdropRepeatsWith=_curry2(function(t,e){return new XDropRepeatsWith(t,e)});module.exports=_xdropRepeatsWith;

},{"./_curry2":108,"./_xfBase":154}],153:[function(require,module,exports){
var _curry2=require("./_curry2"),_xfBase=require("./_xfBase"),XDropWhile=function(){function r(r,t){this.xf=t,this.f=r}return r.prototype["@@transducer/init"]=_xfBase.init,r.prototype["@@transducer/result"]=_xfBase.result,r.prototype["@@transducer/step"]=function(r,t){if(this.f){if(this.f(t))return r;this.f=null}return this.xf["@@transducer/step"](r,t)},r}(),_xdropWhile=_curry2(function(r,t){return new XDropWhile(r,t)});module.exports=_xdropWhile;

},{"./_curry2":108,"./_xfBase":154}],154:[function(require,module,exports){
module.exports={init:function(){return this.xf["@@transducer/init"]()},result:function(t){return this.xf["@@transducer/result"](t)}};

},{}],155:[function(require,module,exports){
var _curry2=require("./_curry2"),_xfBase=require("./_xfBase"),XFilter=function(){function r(r,t){this.xf=t,this.f=r}return r.prototype["@@transducer/init"]=_xfBase.init,r.prototype["@@transducer/result"]=_xfBase.result,r.prototype["@@transducer/step"]=function(r,t){return this.f(t)?this.xf["@@transducer/step"](r,t):r},r}(),_xfilter=_curry2(function(r,t){return new XFilter(r,t)});module.exports=_xfilter;

},{"./_curry2":108,"./_xfBase":154}],156:[function(require,module,exports){
var _curry2=require("./_curry2"),_reduced=require("./_reduced"),_xfBase=require("./_xfBase"),XFind=function(){function r(r,t){this.xf=t,this.f=r,this.found=!1}return r.prototype["@@transducer/init"]=_xfBase.init,r.prototype["@@transducer/result"]=function(r){return this.found||(r=this.xf["@@transducer/step"](r,void 0)),this.xf["@@transducer/result"](r)},r.prototype["@@transducer/step"]=function(r,t){return this.f(t)&&(this.found=!0,r=_reduced(this.xf["@@transducer/step"](r,t))),r},r}(),_xfind=_curry2(function(r,t){return new XFind(r,t)});module.exports=_xfind;

},{"./_curry2":108,"./_reduced":141,"./_xfBase":154}],157:[function(require,module,exports){
var _curry2=require("./_curry2"),_reduced=require("./_reduced"),_xfBase=require("./_xfBase"),XFindIndex=function(){function r(r,t){this.xf=t,this.f=r,this.idx=-1,this.found=!1}return r.prototype["@@transducer/init"]=_xfBase.init,r.prototype["@@transducer/result"]=function(r){return this.found||(r=this.xf["@@transducer/step"](r,-1)),this.xf["@@transducer/result"](r)},r.prototype["@@transducer/step"]=function(r,t){return this.idx+=1,this.f(t)&&(this.found=!0,r=_reduced(this.xf["@@transducer/step"](r,this.idx))),r},r}(),_xfindIndex=_curry2(function(r,t){return new XFindIndex(r,t)});module.exports=_xfindIndex;

},{"./_curry2":108,"./_reduced":141,"./_xfBase":154}],158:[function(require,module,exports){
var _curry2=require("./_curry2"),_xfBase=require("./_xfBase"),XFindLast=function(){function t(t,r){this.xf=r,this.f=t}return t.prototype["@@transducer/init"]=_xfBase.init,t.prototype["@@transducer/result"]=function(t){return this.xf["@@transducer/result"](this.xf["@@transducer/step"](t,this.last))},t.prototype["@@transducer/step"]=function(t,r){return this.f(r)&&(this.last=r),t},t}(),_xfindLast=_curry2(function(t,r){return new XFindLast(t,r)});module.exports=_xfindLast;

},{"./_curry2":108,"./_xfBase":154}],159:[function(require,module,exports){
var _curry2=require("./_curry2"),_xfBase=require("./_xfBase"),XFindLastIndex=function(){function t(t,r){this.xf=r,this.f=t,this.idx=-1,this.lastIdx=-1}return t.prototype["@@transducer/init"]=_xfBase.init,t.prototype["@@transducer/result"]=function(t){return this.xf["@@transducer/result"](this.xf["@@transducer/step"](t,this.lastIdx))},t.prototype["@@transducer/step"]=function(t,r){return this.idx+=1,this.f(r)&&(this.lastIdx=this.idx),t},t}(),_xfindLastIndex=_curry2(function(t,r){return new XFindLastIndex(t,r)});module.exports=_xfindLastIndex;

},{"./_curry2":108,"./_xfBase":154}],160:[function(require,module,exports){
var _curry2=require("./_curry2"),_xfBase=require("./_xfBase"),XMap=function(){function r(r,t){this.xf=t,this.f=r}return r.prototype["@@transducer/init"]=_xfBase.init,r.prototype["@@transducer/result"]=_xfBase.result,r.prototype["@@transducer/step"]=function(r,t){return this.xf["@@transducer/step"](r,this.f(t))},r}(),_xmap=_curry2(function(r,t){return new XMap(r,t)});module.exports=_xmap;

},{"./_curry2":108,"./_xfBase":154}],161:[function(require,module,exports){
var _curryN=require("./_curryN"),_has=require("./_has"),_xfBase=require("./_xfBase"),XReduceBy=function(){function t(t,e,r,s){this.valueFn=t,this.valueAcc=e,this.keyFn=r,this.xf=s,this.inputs={}}return t.prototype["@@transducer/init"]=_xfBase.init,t.prototype["@@transducer/result"]=function(t){var e;for(e in this.inputs)if(_has(e,this.inputs)&&(t=this.xf["@@transducer/step"](t,this.inputs[e]))["@@transducer/reduced"]){t=t["@@transducer/value"];break}return this.inputs=null,this.xf["@@transducer/result"](t)},t.prototype["@@transducer/step"]=function(t,e){var r=this.keyFn(e);return this.inputs[r]=this.inputs[r]||[r,this.valueAcc],this.inputs[r][1]=this.valueFn(this.inputs[r][1],e),t},t}(),_xreduceBy=_curryN(4,[],function(t,e,r,s){return new XReduceBy(t,e,r,s)});module.exports=_xreduceBy;

},{"./_curryN":110,"./_has":119,"./_xfBase":154}],162:[function(require,module,exports){
var _curry2=require("./_curry2"),_reduced=require("./_reduced"),_xfBase=require("./_xfBase"),XTake=function(){function e(e,r){this.xf=r,this.n=e,this.i=0}return e.prototype["@@transducer/init"]=_xfBase.init,e.prototype["@@transducer/result"]=_xfBase.result,e.prototype["@@transducer/step"]=function(e,r){this.i+=1;var t=0===this.n?e:this.xf["@@transducer/step"](e,r);return this.n>=0&&this.i>=this.n?_reduced(t):t},e}(),_xtake=_curry2(function(e,r){return new XTake(e,r)});module.exports=_xtake;

},{"./_curry2":108,"./_reduced":141,"./_xfBase":154}],163:[function(require,module,exports){
var _curry2=require("./_curry2"),_reduced=require("./_reduced"),_xfBase=require("./_xfBase"),XTakeWhile=function(){function e(e,r){this.xf=r,this.f=e}return e.prototype["@@transducer/init"]=_xfBase.init,e.prototype["@@transducer/result"]=_xfBase.result,e.prototype["@@transducer/step"]=function(e,r){return this.f(r)?this.xf["@@transducer/step"](e,r):_reduced(e)},e}(),_xtakeWhile=_curry2(function(e,r){return new XTakeWhile(e,r)});module.exports=_xtakeWhile;

},{"./_curry2":108,"./_reduced":141,"./_xfBase":154}],164:[function(require,module,exports){
var _curry2=require("./_curry2"),_xfBase=require("./_xfBase"),XTap=function(){function r(r,t){this.xf=t,this.f=r}return r.prototype["@@transducer/init"]=_xfBase.init,r.prototype["@@transducer/result"]=_xfBase.result,r.prototype["@@transducer/step"]=function(r,t){return this.f(t),this.xf["@@transducer/step"](r,t)},r}(),_xtap=_curry2(function(r,t){return new XTap(r,t)});module.exports=_xtap;

},{"./_curry2":108,"./_xfBase":154}],165:[function(require,module,exports){
var XWrap=function(){function r(r){this.f=r}return r.prototype["@@transducer/init"]=function(){throw new Error("init not implemented on XWrap")},r.prototype["@@transducer/result"]=function(r){return r},r.prototype["@@transducer/step"]=function(r,t){return this.f(r,t)},r}();function _xwrap(r){return new XWrap(r)}module.exports=_xwrap;

},{}],166:[function(require,module,exports){
var _contains=require("./internal/_contains"),_curry2=require("./internal/_curry2"),_filter=require("./internal/_filter"),flip=require("./flip"),uniq=require("./uniq"),intersection=_curry2(function(r,e){var i,n;return r.length>e.length?(i=r,n=e):(i=e,n=r),uniq(_filter(flip(_contains)(i),n))});module.exports=intersection;

},{"./flip":72,"./internal/_contains":104,"./internal/_curry2":108,"./internal/_filter":115,"./uniq":303}],167:[function(require,module,exports){
var _checkForMethod=require("./internal/_checkForMethod"),_curry2=require("./internal/_curry2"),intersperse=_curry2(_checkForMethod("intersperse",function(r,e){for(var t=[],n=0,c=e.length;n<c;)n===c-1?t.push(e[n]):t.push(e[n],r),n+=1;return t}));module.exports=intersperse;

},{"./internal/_checkForMethod":99,"./internal/_curry2":108}],168:[function(require,module,exports){
var _clone=require("./internal/_clone"),_curry3=require("./internal/_curry3"),_isTransformer=require("./internal/_isTransformer"),_reduce=require("./internal/_reduce"),_stepCat=require("./internal/_stepCat"),into=_curry3(function(r,e,n){return _isTransformer(r)?_reduce(e(r),r["@@transducer/init"](),n):_reduce(e(_stepCat(r)),_clone(r,[],[],!1),n)});module.exports=into;

},{"./internal/_clone":100,"./internal/_curry3":109,"./internal/_isTransformer":132,"./internal/_reduce":140,"./internal/_stepCat":142}],169:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_has=require("./internal/_has"),keys=require("./keys"),invert=_curry1(function(r){for(var e=keys(r),n=e.length,t=0,u={};t<n;){var a=e[t],i=r[a],s=_has(i,u)?u[i]:u[i]=[];s[s.length]=a,t+=1}return u});module.exports=invert;

},{"./internal/_curry1":107,"./internal/_has":119,"./keys":177}],170:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),keys=require("./keys"),invertObj=_curry1(function(r){for(var e=keys(r),n=e.length,u=0,t={};u<n;){var i=e[u];t[r[i]]=i,u+=1}return t});module.exports=invertObj;

},{"./internal/_curry1":107,"./keys":177}],171:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_isFunction=require("./internal/_isFunction"),curryN=require("./curryN"),toString=require("./toString"),invoker=_curry2(function(r,n){return curryN(r+1,function(){var e=arguments[r];if(null!=e&&_isFunction(e[n]))return e[n].apply(e,Array.prototype.slice.call(arguments,0,r));throw new TypeError(toString(e)+' does not have a method named "'+n+'"')})});module.exports=invoker;

},{"./curryN":44,"./internal/_curry2":108,"./internal/_isFunction":125,"./toString":289}],172:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),is=_curry2(function(r,n){return null!=n&&n.constructor===r||n instanceof r});module.exports=is;

},{"./internal/_curry2":108}],173:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),empty=require("./empty"),equals=require("./equals"),isEmpty=_curry1(function(r){return null!=r&&equals(r,empty(r))});module.exports=isEmpty;

},{"./empty":60,"./equals":64,"./internal/_curry1":107}],174:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),isNil=_curry1(function(r){return null==r});module.exports=isNil;

},{"./internal/_curry1":107}],175:[function(require,module,exports){
var invoker=require("./invoker"),join=invoker(1,"join");module.exports=join;

},{"./invoker":171}],176:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),converge=require("./converge"),juxt=_curry1(function(r){return converge(function(){return Array.prototype.slice.call(arguments,0)},r)});module.exports=juxt;

},{"./converge":41,"./internal/_curry1":107}],177:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_has=require("./internal/_has"),_isArguments=require("./internal/_isArguments"),hasEnumBug=!{toString:null}.propertyIsEnumerable("toString"),nonEnumerableProps=["constructor","valueOf","isPrototypeOf","toString","propertyIsEnumerable","hasOwnProperty","toLocaleString"],hasArgsEnumBug=function(){"use strict";return arguments.propertyIsEnumerable("length")}(),contains=function(r,n){for(var e=0;e<r.length;){if(r[e]===n)return!0;e+=1}return!1},_keys="function"!=typeof Object.keys||hasArgsEnumBug?function(r){if(Object(r)!==r)return[];var n,e,t=[],u=hasArgsEnumBug&&_isArguments(r);for(n in r)!_has(n,r)||u&&"length"===n||(t[t.length]=n);if(hasEnumBug)for(e=nonEnumerableProps.length-1;e>=0;)n=nonEnumerableProps[e],_has(n,r)&&!contains(t,n)&&(t[t.length]=n),e-=1;return t}:function(r){return Object(r)!==r?[]:Object.keys(r)},keys=_curry1(_keys);module.exports=keys;

},{"./internal/_curry1":107,"./internal/_has":119,"./internal/_isArguments":122}],178:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),keysIn=_curry1(function(r){var e,n=[];for(e in r)n[n.length]=e;return n});module.exports=keysIn;

},{"./internal/_curry1":107}],179:[function(require,module,exports){
var nth=require("./nth"),last=nth(-1);module.exports=last;

},{"./nth":218}],180:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_isArray=require("./internal/_isArray"),equals=require("./equals"),lastIndexOf=_curry2(function(r,e){if("function"!=typeof e.lastIndexOf||_isArray(e)){for(var n=e.length-1;n>=0;){if(equals(e[n],r))return n;n-=1}return-1}return e.lastIndexOf(r)});module.exports=lastIndexOf;

},{"./equals":64,"./internal/_curry2":108,"./internal/_isArray":123}],181:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_isNumber=require("./internal/_isNumber"),length=_curry1(function(r){return null!=r&&_isNumber(r.length)?r.length:NaN});module.exports=length;

},{"./internal/_curry1":107,"./internal/_isNumber":127}],182:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),map=require("./map"),lens=_curry2(function(r,n){return function(u){return function(e){return map(function(r){return n(r,e)},u(r(e)))}}});module.exports=lens;

},{"./internal/_curry2":108,"./map":190}],183:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),lens=require("./lens"),nth=require("./nth"),update=require("./update"),lensIndex=_curry1(function(e){return lens(nth(e),update(e))});module.exports=lensIndex;

},{"./internal/_curry1":107,"./lens":182,"./nth":218,"./update":309}],184:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),assocPath=require("./assocPath"),lens=require("./lens"),path=require("./path"),lensPath=_curry1(function(r){return lens(path(r),assocPath(r))});module.exports=lensPath;

},{"./assocPath":23,"./internal/_curry1":107,"./lens":182,"./path":231}],185:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),assoc=require("./assoc"),lens=require("./lens"),prop=require("./prop"),lensProp=_curry1(function(r){return lens(prop(r),assoc(r))});module.exports=lensProp;

},{"./assoc":22,"./internal/_curry1":107,"./lens":182,"./prop":245}],186:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),liftN=require("./liftN"),lift=_curry1(function(r){return liftN(r.length,r)});module.exports=lift;

},{"./internal/_curry1":107,"./liftN":187}],187:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_reduce=require("./internal/_reduce"),ap=require("./ap"),curryN=require("./curryN"),map=require("./map"),liftN=_curry2(function(r,e){var u=curryN(r,e);return curryN(r,function(){return _reduce(ap,map(u,arguments[0]),Array.prototype.slice.call(arguments,1))})});module.exports=liftN;

},{"./ap":15,"./curryN":44,"./internal/_curry2":108,"./internal/_reduce":140,"./map":190}],188:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),lt=_curry2(function(r,u){return r<u});module.exports=lt;

},{"./internal/_curry2":108}],189:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),lte=_curry2(function(r,e){return r<=e});module.exports=lte;

},{"./internal/_curry2":108}],190:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_map=require("./internal/_map"),_reduce=require("./internal/_reduce"),_xmap=require("./internal/_xmap"),curryN=require("./curryN"),keys=require("./keys"),map=_curry2(_dispatchable(["fantasy-land/map","map"],_xmap,function(r,e){switch(Object.prototype.toString.call(e)){case"[object Function]":return curryN(e.length,function(){return r.call(this,e.apply(this,arguments))});case"[object Object]":return _reduce(function(t,a){return t[a]=r(e[a]),t},{},keys(e));default:return _map(r,e)}}));module.exports=map;

},{"./curryN":44,"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_map":134,"./internal/_reduce":140,"./internal/_xmap":160,"./keys":177}],191:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),mapAccum=_curry3(function(r,u,c){for(var e=0,n=c.length,a=[],m=[u];e<n;)m=r(m[0],c[e]),a[e]=m[1],e+=1;return[m[0],a]});module.exports=mapAccum;

},{"./internal/_curry3":109}],192:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),mapAccumRight=_curry3(function(r,u,c){for(var e=c.length-1,t=[],n=[u];e>=0;)n=r(c[e],n[0]),t[e]=n[1],e-=1;return[t,n[0]]});module.exports=mapAccumRight;

},{"./internal/_curry3":109}],193:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_reduce=require("./internal/_reduce"),keys=require("./keys"),mapObjIndexed=_curry2(function(e,r){return _reduce(function(u,n){return u[n]=e(r[n],n,r),u},{},keys(r))});module.exports=mapObjIndexed;

},{"./internal/_curry2":108,"./internal/_reduce":140,"./keys":177}],194:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),match=_curry2(function(r,c){return c.match(r)||[]});module.exports=match;

},{"./internal/_curry2":108}],195:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_isInteger=require("./internal/_isInteger"),mathMod=_curry2(function(r,e){return _isInteger(r)?!_isInteger(e)||e<1?NaN:(r%e+e)%e:NaN});module.exports=mathMod;

},{"./internal/_curry2":108,"./internal/_isInteger":126}],196:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),max=_curry2(function(r,u){return u>r?u:r});module.exports=max;

},{"./internal/_curry2":108}],197:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),maxBy=_curry3(function(r,u,e){return r(e)>r(u)?e:u});module.exports=maxBy;

},{"./internal/_curry3":109}],198:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),sum=require("./sum"),mean=_curry1(function(r){return sum(r)/r.length});module.exports=mean;

},{"./internal/_curry1":107,"./sum":275}],199:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),mean=require("./mean"),median=_curry1(function(r){var e=r.length;if(0===e)return NaN;var n=2-e%2,a=(e-n)/2;return mean(Array.prototype.slice.call(r,0).sort(function(r,e){return r<e?-1:r>e?1:0}).slice(a,a+n))});module.exports=median;

},{"./internal/_curry1":107,"./mean":198}],200:[function(require,module,exports){
var memoizeWith=require("./memoizeWith"),toString=require("./toString"),memoize=memoizeWith(function(){return toString(arguments)});module.exports=memoize;

},{"./memoizeWith":201,"./toString":289}],201:[function(require,module,exports){
var _arity=require("./internal/_arity"),_curry2=require("./internal/_curry2"),_has=require("./internal/_has"),memoizeWith=_curry2(function(r,e){var i={};return _arity(e.length,function(){var t=r.apply(this,arguments);return _has(t,i)||(i[t]=e.apply(this,arguments)),i[t]})});module.exports=memoizeWith;

},{"./internal/_arity":96,"./internal/_curry2":108,"./internal/_has":119}],202:[function(require,module,exports){
var _assign=require("./internal/_assign"),_curry2=require("./internal/_curry2"),merge=_curry2(function(r,e){return _assign({},r,e)});module.exports=merge;

},{"./internal/_assign":98,"./internal/_curry2":108}],203:[function(require,module,exports){
var _assign=require("./internal/_assign"),_curry1=require("./internal/_curry1"),mergeAll=_curry1(function(r){return _assign.apply(null,[{}].concat(r))});module.exports=mergeAll;

},{"./internal/_assign":98,"./internal/_curry1":107}],204:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),mergeDeepWithKey=require("./mergeDeepWithKey"),mergeDeepLeft=_curry2(function(e,r){return mergeDeepWithKey(function(e,r,t){return r},e,r)});module.exports=mergeDeepLeft;

},{"./internal/_curry2":108,"./mergeDeepWithKey":207}],205:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),mergeDeepWithKey=require("./mergeDeepWithKey"),mergeDeepRight=_curry2(function(e,r){return mergeDeepWithKey(function(e,r,t){return t},e,r)});module.exports=mergeDeepRight;

},{"./internal/_curry2":108,"./mergeDeepWithKey":207}],206:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),mergeDeepWithKey=require("./mergeDeepWithKey"),mergeDeepWith=_curry3(function(e,r,t){return mergeDeepWithKey(function(r,t,i){return e(t,i)},r,t)});module.exports=mergeDeepWith;

},{"./internal/_curry3":109,"./mergeDeepWithKey":207}],207:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),_isObject=require("./internal/_isObject"),mergeWithKey=require("./mergeWithKey"),mergeDeepWithKey=_curry3(function e(r,i,t){return mergeWithKey(function(i,t,u){return _isObject(t)&&_isObject(u)?e(r,t,u):r(i,t,u)},i,t)});module.exports=mergeDeepWithKey;

},{"./internal/_curry3":109,"./internal/_isObject":128,"./mergeWithKey":209}],208:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),mergeWithKey=require("./mergeWithKey"),mergeWith=_curry3(function(e,r,t){return mergeWithKey(function(r,t,i){return e(t,i)},r,t)});module.exports=mergeWith;

},{"./internal/_curry3":109,"./mergeWithKey":209}],209:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),_has=require("./internal/_has"),mergeWithKey=_curry3(function(r,e,a){var i,n={};for(i in e)_has(i,e)&&(n[i]=_has(i,a)?r(i,e[i],a[i]):e[i]);for(i in a)_has(i,a)&&!_has(i,n)&&(n[i]=a[i]);return n});module.exports=mergeWithKey;

},{"./internal/_curry3":109,"./internal/_has":119}],210:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),min=_curry2(function(r,n){return n<r?n:r});module.exports=min;

},{"./internal/_curry2":108}],211:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),minBy=_curry3(function(r,n,u){return r(u)<r(n)?u:n});module.exports=minBy;

},{"./internal/_curry3":109}],212:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),modulo=_curry2(function(r,u){return r%u});module.exports=modulo;

},{"./internal/_curry2":108}],213:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),multiply=_curry2(function(r,u){return r*u});module.exports=multiply;

},{"./internal/_curry2":108}],214:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),nAry=_curry2(function(r,n){switch(r){case 0:return function(){return n.call(this)};case 1:return function(r){return n.call(this,r)};case 2:return function(r,t){return n.call(this,r,t)};case 3:return function(r,t,e){return n.call(this,r,t,e)};case 4:return function(r,t,e,u){return n.call(this,r,t,e,u)};case 5:return function(r,t,e,u,c){return n.call(this,r,t,e,u,c)};case 6:return function(r,t,e,u,c,a){return n.call(this,r,t,e,u,c,a)};case 7:return function(r,t,e,u,c,a,i){return n.call(this,r,t,e,u,c,a,i)};case 8:return function(r,t,e,u,c,a,i,s){return n.call(this,r,t,e,u,c,a,i,s)};case 9:return function(r,t,e,u,c,a,i,s,l){return n.call(this,r,t,e,u,c,a,i,s,l)};case 10:return function(r,t,e,u,c,a,i,s,l,o){return n.call(this,r,t,e,u,c,a,i,s,l,o)};default:throw new Error("First argument to nAry must be a non-negative integer no greater than ten")}});module.exports=nAry;

},{"./internal/_curry2":108}],215:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),negate=_curry1(function(r){return-r});module.exports=negate;

},{"./internal/_curry1":107}],216:[function(require,module,exports){
var _complement=require("./internal/_complement"),_curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xany=require("./internal/_xany"),any=require("./any"),none=_curry2(_complement(_dispatchable(["any"],_xany,any)));module.exports=none;

},{"./any":13,"./internal/_complement":102,"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xany":146}],217:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),not=_curry1(function(r){return!r});module.exports=not;

},{"./internal/_curry1":107}],218:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_isString=require("./internal/_isString"),nth=_curry2(function(r,n){var t=r<0?n.length+r:r;return _isString(n)?n.charAt(t):n[t]});module.exports=nth;

},{"./internal/_curry2":108,"./internal/_isString":131}],219:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),curryN=require("./curryN"),nth=require("./nth"),nthArg=_curry1(function(r){return curryN(r<0?1:r+1,function(){return nth(r,arguments)})});module.exports=nthArg;

},{"./curryN":44,"./internal/_curry1":107,"./nth":218}],220:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),o=_curry3(function(r,u,e){return r(u(e))});module.exports=o;

},{"./internal/_curry3":109}],221:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),objOf=_curry2(function(r,u){var e={};return e[r]=u,e});module.exports=objOf;

},{"./internal/_curry2":108}],222:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_of=require("./internal/_of"),of=_curry1(_of);module.exports=of;

},{"./internal/_curry1":107,"./internal/_of":136}],223:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),omit=_curry2(function(r,e){for(var n={},o={},t=0,u=r.length;t<u;)o[r[t]]=1,t+=1;for(var i in e)o.hasOwnProperty(i)||(n[i]=e[i]);return n});module.exports=omit;

},{"./internal/_curry2":108}],224:[function(require,module,exports){
var _arity=require("./internal/_arity"),_curry1=require("./internal/_curry1"),once=_curry1(function(r){var e,n=!1;return _arity(r.length,function(){return n?e:(n=!0,e=r.apply(this,arguments))})});module.exports=once;

},{"./internal/_arity":96,"./internal/_curry1":107}],225:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),or=_curry2(function(r,u){return r||u});module.exports=or;

},{"./internal/_curry2":108}],226:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),Identity=function(r){return{value:r,map:function(n){return Identity(n(r))}}},over=_curry3(function(r,n,e){return r(function(r){return Identity(n(r))})(e).value});module.exports=over;

},{"./internal/_curry3":109}],227:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),pair=_curry2(function(r,u){return[r,u]});module.exports=pair;

},{"./internal/_curry2":108}],228:[function(require,module,exports){
var _concat=require("./internal/_concat"),_createPartialApplicator=require("./internal/_createPartialApplicator"),partial=_createPartialApplicator(_concat);module.exports=partial;

},{"./internal/_concat":103,"./internal/_createPartialApplicator":106}],229:[function(require,module,exports){
var _concat=require("./internal/_concat"),_createPartialApplicator=require("./internal/_createPartialApplicator"),flip=require("./flip"),partialRight=_createPartialApplicator(flip(_concat));module.exports=partialRight;

},{"./flip":72,"./internal/_concat":103,"./internal/_createPartialApplicator":106}],230:[function(require,module,exports){
var filter=require("./filter"),juxt=require("./juxt"),reject=require("./reject"),partition=juxt([filter,reject]);module.exports=partition;

},{"./filter":66,"./juxt":176,"./reject":257}],231:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),path=_curry2(function(r,u){for(var e=u,n=0;n<r.length;){if(null==e)return;e=e[r[n]],n+=1}return e});module.exports=path;

},{"./internal/_curry2":108}],232:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),equals=require("./equals"),path=require("./path"),pathEq=_curry3(function(r,e,u){return equals(path(r,u),e)});module.exports=pathEq;

},{"./equals":64,"./internal/_curry3":109,"./path":231}],233:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),defaultTo=require("./defaultTo"),path=require("./path"),pathOr=_curry3(function(r,e,t){return defaultTo(r,path(e,t))});module.exports=pathOr;

},{"./defaultTo":46,"./internal/_curry3":109,"./path":231}],234:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),path=require("./path"),pathSatisfies=_curry3(function(r,t,e){return t.length>0&&r(path(t,e))});module.exports=pathSatisfies;

},{"./internal/_curry3":109,"./path":231}],235:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),pick=_curry2(function(r,e){for(var n={},u=0;u<r.length;)r[u]in e&&(n[r[u]]=e[r[u]]),u+=1;return n});module.exports=pick;

},{"./internal/_curry2":108}],236:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),pickAll=_curry2(function(r,e){for(var l={},u=0,c=r.length;u<c;){var n=r[u];l[n]=e[n],u+=1}return l});module.exports=pickAll;

},{"./internal/_curry2":108}],237:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),pickBy=_curry2(function(r,u){var c={};for(var e in u)r(u[e],e,u)&&(c[e]=u[e]);return c});module.exports=pickBy;

},{"./internal/_curry2":108}],238:[function(require,module,exports){
var _arity=require("./internal/_arity"),_pipe=require("./internal/_pipe"),reduce=require("./reduce"),tail=require("./tail");function pipe(){if(0===arguments.length)throw new Error("pipe requires at least one argument");return _arity(arguments[0].length,reduce(_pipe,arguments[0],tail(arguments)))}module.exports=pipe;

},{"./internal/_arity":96,"./internal/_pipe":137,"./reduce":252,"./tail":278}],239:[function(require,module,exports){
var composeK=require("./composeK"),reverse=require("./reverse");function pipeK(){if(0===arguments.length)throw new Error("pipeK requires at least one argument");return composeK.apply(this,reverse(arguments))}module.exports=pipeK;

},{"./composeK":34,"./reverse":261}],240:[function(require,module,exports){
var _arity=require("./internal/_arity"),_pipeP=require("./internal/_pipeP"),reduce=require("./reduce"),tail=require("./tail");function pipeP(){if(0===arguments.length)throw new Error("pipeP requires at least one argument");return _arity(arguments[0].length,reduce(_pipeP,arguments[0],tail(arguments)))}module.exports=pipeP;

},{"./internal/_arity":96,"./internal/_pipeP":138,"./reduce":252,"./tail":278}],241:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),map=require("./map"),prop=require("./prop"),pluck=_curry2(function(r,p){return map(prop(r),p)});module.exports=pluck;

},{"./internal/_curry2":108,"./map":190,"./prop":245}],242:[function(require,module,exports){
var _concat=require("./internal/_concat"),_curry2=require("./internal/_curry2"),prepend=_curry2(function(r,e){return _concat([r],e)});module.exports=prepend;

},{"./internal/_concat":103,"./internal/_curry2":108}],243:[function(require,module,exports){
var multiply=require("./multiply"),reduce=require("./reduce"),product=reduce(multiply,1);module.exports=product;

},{"./multiply":213,"./reduce":252}],244:[function(require,module,exports){
var _map=require("./internal/_map"),identity=require("./identity"),pickAll=require("./pickAll"),useWith=require("./useWith"),project=useWith(_map,[pickAll,identity]);module.exports=project;

},{"./identity":84,"./internal/_map":134,"./pickAll":236,"./useWith":310}],245:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),path=require("./path"),prop=_curry2(function(r,e){return path([r],e)});module.exports=prop;

},{"./internal/_curry2":108,"./path":231}],246:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),equals=require("./equals"),propEq=_curry3(function(r,e,u){return equals(e,u[r])});module.exports=propEq;

},{"./equals":64,"./internal/_curry3":109}],247:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),is=require("./is"),propIs=_curry3(function(r,e,u){return is(r,u[e])});module.exports=propIs;

},{"./internal/_curry3":109,"./is":172}],248:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),_has=require("./internal/_has"),propOr=_curry3(function(r,e,u){return null!=u&&_has(e,u)?u[e]:r});module.exports=propOr;

},{"./internal/_curry3":109,"./internal/_has":119}],249:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),propSatisfies=_curry3(function(r,e,i){return r(i[e])});module.exports=propSatisfies;

},{"./internal/_curry3":109}],250:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),props=_curry2(function(r,e){for(var u=r.length,n=[],o=0;o<u;)n[o]=e[r[o]],o+=1;return n});module.exports=props;

},{"./internal/_curry2":108}],251:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_isNumber=require("./internal/_isNumber"),range=_curry2(function(r,e){if(!_isNumber(r)||!_isNumber(e))throw new TypeError("Both arguments to range must be numbers");for(var u=[],n=r;n<e;)u.push(n),n+=1;return u});module.exports=range;

},{"./internal/_curry2":108,"./internal/_isNumber":127}],252:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),_reduce=require("./internal/_reduce"),reduce=_curry3(_reduce);module.exports=reduce;

},{"./internal/_curry3":109,"./internal/_reduce":140}],253:[function(require,module,exports){
var _curryN=require("./internal/_curryN"),_dispatchable=require("./internal/_dispatchable"),_has=require("./internal/_has"),_reduce=require("./internal/_reduce"),_xreduceBy=require("./internal/_xreduceBy"),reduceBy=_curryN(4,[],_dispatchable([],_xreduceBy,function(e,r,u,a){return _reduce(function(a,c){var n=u(c);return a[n]=e(_has(n,a)?a[n]:r,c),a},{},a)}));module.exports=reduceBy;

},{"./internal/_curryN":110,"./internal/_dispatchable":111,"./internal/_has":119,"./internal/_reduce":140,"./internal/_xreduceBy":161}],254:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),reduceRight=_curry3(function(r,e,u){for(var t=u.length-1;t>=0;)e=r(u[t],e),t-=1;return e});module.exports=reduceRight;

},{"./internal/_curry3":109}],255:[function(require,module,exports){
var _curryN=require("./internal/_curryN"),_reduce=require("./internal/_reduce"),_reduced=require("./internal/_reduced"),reduceWhile=_curryN(4,[],function(e,r,u,c){return _reduce(function(u,c){return e(u,c)?r(u,c):_reduced(u)},u,c)});module.exports=reduceWhile;

},{"./internal/_curryN":110,"./internal/_reduce":140,"./internal/_reduced":141}],256:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_reduced=require("./internal/_reduced"),reduced=_curry1(_reduced);module.exports=reduced;

},{"./internal/_curry1":107,"./internal/_reduced":141}],257:[function(require,module,exports){
var _complement=require("./internal/_complement"),_curry2=require("./internal/_curry2"),filter=require("./filter"),reject=_curry2(function(e,r){return filter(_complement(e),r)});module.exports=reject;

},{"./filter":66,"./internal/_complement":102,"./internal/_curry2":108}],258:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),remove=_curry3(function(r,e,c){var o=Array.prototype.slice.call(c,0);return o.splice(r,e),o});module.exports=remove;

},{"./internal/_curry3":109}],259:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),always=require("./always"),times=require("./times"),repeat=_curry2(function(r,e){return times(always(r),e)});module.exports=repeat;

},{"./always":11,"./internal/_curry2":108,"./times":285}],260:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),replace=_curry3(function(r,e,c){return c.replace(r,e)});module.exports=replace;

},{"./internal/_curry3":109}],261:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_isString=require("./internal/_isString"),reverse=_curry1(function(r){return _isString(r)?r.split("").reverse().join(""):Array.prototype.slice.call(r,0).reverse()});module.exports=reverse;

},{"./internal/_curry1":107,"./internal/_isString":131}],262:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),scan=_curry3(function(r,n,e){for(var u=0,c=e.length,a=[n];u<c;)n=r(n,e[u]),a[u+1]=n,u+=1;return a});module.exports=scan;

},{"./internal/_curry3":109}],263:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),ap=require("./ap"),map=require("./map"),prepend=require("./prepend"),reduceRight=require("./reduceRight"),sequence=_curry2(function(e,r){return"function"==typeof r.sequence?r.sequence(e):reduceRight(function(e,r){return ap(map(prepend,e),r)},e([]),r)});module.exports=sequence;

},{"./ap":15,"./internal/_curry2":108,"./map":190,"./prepend":242,"./reduceRight":254}],264:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),always=require("./always"),over=require("./over"),set=_curry3(function(r,e,u){return over(r,always(e),u)});module.exports=set;

},{"./always":11,"./internal/_curry3":109,"./over":226}],265:[function(require,module,exports){
var _checkForMethod=require("./internal/_checkForMethod"),_curry3=require("./internal/_curry3"),slice=_curry3(_checkForMethod("slice",function(r,e,c){return Array.prototype.slice.call(c,r,e)}));module.exports=slice;

},{"./internal/_checkForMethod":99,"./internal/_curry3":109}],266:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),sort=_curry2(function(r,t){return Array.prototype.slice.call(t,0).sort(r)});module.exports=sort;

},{"./internal/_curry2":108}],267:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),sortBy=_curry2(function(r,t){return Array.prototype.slice.call(t,0).sort(function(t,e){var o=r(t),u=r(e);return o<u?-1:o>u?1:0})});module.exports=sortBy;

},{"./internal/_curry2":108}],268:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),sortWith=_curry2(function(r,t){return Array.prototype.slice.call(t,0).sort(function(t,e){for(var o=0,n=0;0===o&&n<r.length;)o=r[n](t,e),n+=1;return o})});module.exports=sortWith;

},{"./internal/_curry2":108}],269:[function(require,module,exports){
var invoker=require("./invoker"),split=invoker(1,"split");module.exports=split;

},{"./invoker":171}],270:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),length=require("./length"),slice=require("./slice"),splitAt=_curry2(function(e,r){return[slice(0,e,r),slice(e,length(r),r)]});module.exports=splitAt;

},{"./internal/_curry2":108,"./length":181,"./slice":265}],271:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),slice=require("./slice"),splitEvery=_curry2(function(r,e){if(r<=0)throw new Error("First argument to splitEvery must be a positive integer");for(var i=[],t=0;t<e.length;)i.push(slice(t,t+=r,e));return i});module.exports=splitEvery;

},{"./internal/_curry2":108,"./slice":265}],272:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),splitWhen=_curry2(function(r,e){for(var t=0,l=e.length,n=[];t<l&&!r(e[t]);)n.push(e[t]),t+=1;return[n,Array.prototype.slice.call(e,t)]});module.exports=splitWhen;

},{"./internal/_curry2":108}],273:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),equals=require("./equals"),take=require("./take"),startsWith=_curry2(function(r,e){return equals(take(r.length,e),r)});module.exports=startsWith;

},{"./equals":64,"./internal/_curry2":108,"./take":279}],274:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),subtract=_curry2(function(r,u){return Number(r)-Number(u)});module.exports=subtract;

},{"./internal/_curry2":108}],275:[function(require,module,exports){
var add=require("./add"),reduce=require("./reduce"),sum=reduce(add,0);module.exports=sum;

},{"./add":6,"./reduce":252}],276:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),concat=require("./concat"),difference=require("./difference"),symmetricDifference=_curry2(function(e,r){return concat(difference(e,r),difference(r,e))});module.exports=symmetricDifference;

},{"./concat":36,"./difference":48,"./internal/_curry2":108}],277:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),concat=require("./concat"),differenceWith=require("./differenceWith"),symmetricDifferenceWith=_curry3(function(e,r,i){return concat(differenceWith(e,r,i),differenceWith(e,i,r))});module.exports=symmetricDifferenceWith;

},{"./concat":36,"./differenceWith":49,"./internal/_curry3":109}],278:[function(require,module,exports){
var _checkForMethod=require("./internal/_checkForMethod"),_curry1=require("./internal/_curry1"),slice=require("./slice"),tail=_curry1(_checkForMethod("tail",slice(1,1/0)));module.exports=tail;

},{"./internal/_checkForMethod":99,"./internal/_curry1":107,"./slice":265}],279:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xtake=require("./internal/_xtake"),slice=require("./slice"),take=_curry2(_dispatchable(["take"],_xtake,function(e,r){return slice(0,e<0?1/0:e,r)}));module.exports=take;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xtake":162,"./slice":265}],280:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),drop=require("./drop"),takeLast=_curry2(function(r,e){return drop(r>=0?e.length-r:0,e)});module.exports=takeLast;

},{"./drop":53,"./internal/_curry2":108}],281:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),slice=require("./slice"),takeLastWhile=_curry2(function(r,e){for(var i=e.length-1;i>=0&&r(e[i]);)i-=1;return slice(i+1,1/0,e)});module.exports=takeLastWhile;

},{"./internal/_curry2":108,"./slice":265}],282:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xtakeWhile=require("./internal/_xtakeWhile"),slice=require("./slice"),takeWhile=_curry2(_dispatchable(["takeWhile"],_xtakeWhile,function(e,r){for(var i=0,a=r.length;i<a&&e(r[i]);)i+=1;return slice(0,i,r)}));module.exports=takeWhile;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xtakeWhile":163,"./slice":265}],283:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_dispatchable=require("./internal/_dispatchable"),_xtap=require("./internal/_xtap"),tap=_curry2(_dispatchable([],_xtap,function(r,a){return r(a),a}));module.exports=tap;

},{"./internal/_curry2":108,"./internal/_dispatchable":111,"./internal/_xtap":164}],284:[function(require,module,exports){
var _cloneRegExp=require("./internal/_cloneRegExp"),_curry2=require("./internal/_curry2"),_isRegExp=require("./internal/_isRegExp"),toString=require("./toString"),test=_curry2(function(e,r){if(!_isRegExp(e))throw new TypeError("‘test’ requires a value of type RegExp as its first argument; received "+toString(e));return _cloneRegExp(e).test(r)});module.exports=test;

},{"./internal/_cloneRegExp":101,"./internal/_curry2":108,"./internal/_isRegExp":130,"./toString":289}],285:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),times=_curry2(function(r,e){var n,u=Number(e),t=0;if(u<0||isNaN(u))throw new RangeError("n must be a non-negative number");for(n=new Array(u);t<u;)n[t]=r(t),t+=1;return n});module.exports=times;

},{"./internal/_curry2":108}],286:[function(require,module,exports){
var invoker=require("./invoker"),toLower=invoker(0,"toLowerCase");module.exports=toLower;

},{"./invoker":171}],287:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_has=require("./internal/_has"),toPairs=_curry1(function(r){var a=[];for(var e in r)_has(e,r)&&(a[a.length]=[e,r[e]]);return a});module.exports=toPairs;

},{"./internal/_curry1":107,"./internal/_has":119}],288:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),toPairsIn=_curry1(function(r){var n=[];for(var e in r)n[n.length]=[e,r[e]];return n});module.exports=toPairsIn;

},{"./internal/_curry1":107}],289:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),_toString=require("./internal/_toString"),toString=_curry1(function(r){return _toString(r,[])});module.exports=toString;

},{"./internal/_curry1":107,"./internal/_toString":144}],290:[function(require,module,exports){
var invoker=require("./invoker"),toUpper=invoker(0,"toUpperCase");module.exports=toUpper;

},{"./invoker":171}],291:[function(require,module,exports){
var _reduce=require("./internal/_reduce"),_xwrap=require("./internal/_xwrap"),curryN=require("./curryN"),transduce=curryN(4,function(r,e,u,n){return _reduce(r("function"==typeof e?_xwrap(e):e),u,n)});module.exports=transduce;

},{"./curryN":44,"./internal/_reduce":140,"./internal/_xwrap":165}],292:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),transpose=_curry1(function(r){for(var e=0,n=[];e<r.length;){for(var o=r[e],t=0;t<o.length;)void 0===n[t]&&(n[t]=[]),n[t].push(o[t]),t+=1;e+=1}return n});module.exports=transpose;

},{"./internal/_curry1":107}],293:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),map=require("./map"),sequence=require("./sequence"),traverse=_curry3(function(e,r,a){return"function"==typeof a["fantasy-land/traverse"]?a["fantasy-land/traverse"](r,e):sequence(e,map(r,a))});module.exports=traverse;

},{"./internal/_curry3":109,"./map":190,"./sequence":263}],294:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),ws="\t\n\v\f\r   ᠎             　\u2028\u2029\ufeff",zeroWidth="​",hasProtoTrim="function"==typeof String.prototype.trim,_trim=hasProtoTrim&&!ws.trim()&&zeroWidth.trim()?function(r){return r.trim()}:function(r){var t=new RegExp("^["+ws+"]["+ws+"]*"),e=new RegExp("["+ws+"]["+ws+"]*$");return r.replace(t,"").replace(e,"")},trim=_curry1(_trim);module.exports=trim;

},{"./internal/_curry1":107}],295:[function(require,module,exports){
var _arity=require("./internal/_arity"),_concat=require("./internal/_concat"),_curry2=require("./internal/_curry2"),tryCatch=_curry2(function(r,t){return _arity(r.length,function(){try{return r.apply(this,arguments)}catch(r){return t.apply(this,_concat([r],arguments))}})});module.exports=tryCatch;

},{"./internal/_arity":96,"./internal/_concat":103,"./internal/_curry2":108}],296:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),type=_curry1(function(r){return null===r?"Null":void 0===r?"Undefined":Object.prototype.toString.call(r).slice(8,-1)});module.exports=type;

},{"./internal/_curry1":107}],297:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),unapply=_curry1(function(r){return function(){return r(Array.prototype.slice.call(arguments,0))}});module.exports=unapply;

},{"./internal/_curry1":107}],298:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),nAry=require("./nAry"),unary=_curry1(function(r){return nAry(1,r)});module.exports=unary;

},{"./internal/_curry1":107,"./nAry":214}],299:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),curryN=require("./curryN"),uncurryN=_curry2(function(r,u){return curryN(r,function(){for(var e,n=1,c=u,t=0;n<=r&&"function"==typeof c;)e=n===r?arguments.length:t+c.length,c=c.apply(this,Array.prototype.slice.call(arguments,t,e)),n+=1,t=e;return c})});module.exports=uncurryN;

},{"./curryN":44,"./internal/_curry2":108}],300:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),unfold=_curry2(function(r,n){for(var u=r(n),e=[];u&&u.length;)e[e.length]=u[0],u=r(u[1]);return e});module.exports=unfold;

},{"./internal/_curry2":108}],301:[function(require,module,exports){
var _concat=require("./internal/_concat"),_curry2=require("./internal/_curry2"),compose=require("./compose"),uniq=require("./uniq"),union=_curry2(compose(uniq,_concat));module.exports=union;

},{"./compose":33,"./internal/_concat":103,"./internal/_curry2":108,"./uniq":303}],302:[function(require,module,exports){
var _concat=require("./internal/_concat"),_curry3=require("./internal/_curry3"),uniqWith=require("./uniqWith"),unionWith=_curry3(function(r,n,i){return uniqWith(r,_concat(n,i))});module.exports=unionWith;

},{"./internal/_concat":103,"./internal/_curry3":109,"./uniqWith":305}],303:[function(require,module,exports){
var identity=require("./identity"),uniqBy=require("./uniqBy"),uniq=uniqBy(identity);module.exports=uniq;

},{"./identity":84,"./uniqBy":304}],304:[function(require,module,exports){
var _Set=require("./internal/_Set"),_curry2=require("./internal/_curry2"),uniqBy=_curry2(function(r,e){for(var n,u,t=new _Set,i=[],_=0;_<e.length;)n=r(u=e[_]),t.add(n)&&i.push(u),_+=1;return i});module.exports=uniqBy;

},{"./internal/_Set":94,"./internal/_curry2":108}],305:[function(require,module,exports){
var _containsWith=require("./internal/_containsWith"),_curry2=require("./internal/_curry2"),uniqWith=_curry2(function(r,n){for(var i,t=0,e=n.length,u=[];t<e;)i=n[t],_containsWith(r,i,u)||(u[u.length]=i),t+=1;return u});module.exports=uniqWith;

},{"./internal/_containsWith":105,"./internal/_curry2":108}],306:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),unless=_curry3(function(r,u,e){return r(e)?e:u(e)});module.exports=unless;

},{"./internal/_curry3":109}],307:[function(require,module,exports){
var _identity=require("./internal/_identity"),chain=require("./chain"),unnest=chain(_identity);module.exports=unnest;

},{"./chain":28,"./internal/_identity":120}],308:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),until=_curry3(function(r,u,n){for(var e=n;!r(e);)e=u(e);return e});module.exports=until;

},{"./internal/_curry3":109}],309:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),adjust=require("./adjust"),always=require("./always"),update=_curry3(function(r,u,a){return adjust(always(u),r,a)});module.exports=update;

},{"./adjust":8,"./always":11,"./internal/_curry3":109}],310:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),curryN=require("./curryN"),useWith=_curry2(function(r,e){return curryN(e.length,function(){for(var t=[],u=0;u<e.length;)t.push(e[u].call(this,arguments[u])),u+=1;return r.apply(this,t.concat(Array.prototype.slice.call(arguments,e.length)))})});module.exports=useWith;

},{"./curryN":44,"./internal/_curry2":108}],311:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),keys=require("./keys"),values=_curry1(function(r){for(var e=keys(r),u=e.length,n=[],s=0;s<u;)n[s]=r[e[s]],s+=1;return n});module.exports=values;

},{"./internal/_curry1":107,"./keys":177}],312:[function(require,module,exports){
var _curry1=require("./internal/_curry1"),valuesIn=_curry1(function(r){var e,n=[];for(e in r)n[n.length]=r[e];return n});module.exports=valuesIn;

},{"./internal/_curry1":107}],313:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),Const=function(r){return{value:r,"fantasy-land/map":function(){return this}}},view=_curry2(function(r,n){return r(Const)(n).value});module.exports=view;

},{"./internal/_curry2":108}],314:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),when=_curry3(function(r,e,n){return r(n)?e(n):n});module.exports=when;

},{"./internal/_curry3":109}],315:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),_has=require("./internal/_has"),where=_curry2(function(r,e){for(var n in r)if(_has(n,r)&&!r[n](e[n]))return!1;return!0});module.exports=where;

},{"./internal/_curry2":108,"./internal/_has":119}],316:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),equals=require("./equals"),map=require("./map"),where=require("./where"),whereEq=_curry2(function(e,r){return where(map(equals,e),r)});module.exports=whereEq;

},{"./equals":64,"./internal/_curry2":108,"./map":190,"./where":315}],317:[function(require,module,exports){
var _contains=require("./internal/_contains"),_curry2=require("./internal/_curry2"),flip=require("./flip"),reject=require("./reject"),without=_curry2(function(r,e){return reject(flip(_contains)(r),e)});module.exports=without;

},{"./flip":72,"./internal/_contains":104,"./internal/_curry2":108,"./reject":257}],318:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),xprod=_curry2(function(r,e){for(var n,o=0,t=r.length,u=e.length,l=[];o<t;){for(n=0;n<u;)l[l.length]=[r[o],e[n]],n+=1;o+=1}return l});module.exports=xprod;

},{"./internal/_curry2":108}],319:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),zip=_curry2(function(r,e){for(var n=[],t=0,u=Math.min(r.length,e.length);t<u;)n[t]=[r[t],e[t]],t+=1;return n});module.exports=zip;

},{"./internal/_curry2":108}],320:[function(require,module,exports){
var _curry2=require("./internal/_curry2"),zipObj=_curry2(function(r,e){for(var n=0,t=Math.min(r.length,e.length),u={};n<t;)u[r[n]]=e[n],n+=1;return u});module.exports=zipObj;

},{"./internal/_curry2":108}],321:[function(require,module,exports){
var _curry3=require("./internal/_curry3"),zipWith=_curry3(function(r,t,e){for(var i=[],n=0,u=Math.min(t.length,e.length);n<u;)i[n]=r(t[n],e[n]),n+=1;return i});module.exports=zipWith;

},{"./internal/_curry3":109}],322:[function(require,module,exports){
const{curryN:curryN}=require("ramda"),captureBase=require("./captureBase"),capture=curryN(2,(r,e)=>captureBase(!1,null,r,e));module.exports=capture;

},{"./captureBase":326,"ramda":87}],323:[function(require,module,exports){
const{curryN:curryN}=require("ramda"),captureBase=require("./captureBase"),captureAll=curryN(2,(r,e)=>captureBase(!0,null,r,e));module.exports=captureAll;

},{"./captureBase":326,"ramda":87}],324:[function(require,module,exports){
const{curryN:curryN}=require("ramda"),captureBase=require("./captureBase"),captureAllAs=curryN(2,(r,e,u)=>captureBase(!0,r,e,u));module.exports=captureAllAs;

},{"./captureBase":326,"ramda":87}],325:[function(require,module,exports){
const{curryN:curryN}=require("ramda"),captureBase=require("./captureBase"),captureAs=curryN(2,(r,e,u)=>captureBase(!1,r,e,u));module.exports=captureAs;

},{"./captureBase":326,"ramda":87}],326:[function(require,module,exports){
const{is:is}=require("ramda"),get=require("./get"),getOr=require("./getOr");function captureBase(e=!0,r=null,t,u){let s=[],g=t.flags||"",i=new RegExp(t.source,t.flags);if(g.includes("g")||(i=new RegExp(t.source,`${g}g`)),is(String,u)){let t,g=!0;for(;g&&null!==(t=i.exec(u));)t&&(is(Array,r)?s.push(r.reduce((e,r,u)=>(e[r]=getOr(null,u,t.slice(1)),e),{})):s.push({input:get("input",t),matched:get("0",t),index:get("index",t),groups:t.slice(1)})),e||(g=!1)}return e?s:getOr(null,"0",s)}module.exports=captureBase;

},{"./get":339,"./getOr":342,"ramda":87}],327:[function(require,module,exports){
const{replace:replace}=require("ramda"),compact=replace(/\s+/gm," ");module.exports=compact;

},{"ramda":87}],328:[function(require,module,exports){
const{ifElse:ifElse,pipe:pipe,is:is,mapObjIndexed:mapObjIndexed,curryN:curryN,map:map,unary:unary}=require("ramda"),digBase=require("./digBase"),dig=curryN(2,(e,i)=>digBase(dig,e,i));module.exports=dig;

},{"./digBase":329,"ramda":87}],329:[function(require,module,exports){
const{ifElse:ifElse,pipe:pipe,is:is,mapObjIndexed:mapObjIndexed,curryN:curryN,map:map,unary:unary}=require("ramda"),mapper=(e,p)=>pipe(p,e(p)),digBase=curryN(3,(e,p,r)=>ifElse(is(Array),map(mapper(e,p)),ifElse(is(Object),mapObjIndexed(mapper(e,p)),unary(p)))(r));module.exports=digBase;

},{"ramda":87}],330:[function(require,module,exports){
const dig=require("./dig"),{flip:flip}=require("ramda"),digFlip=flip(dig);module.exports=digFlip;

},{"./dig":328,"ramda":87}],331:[function(require,module,exports){
const{when:when,ifElse:ifElse,is:is,mapObjIndexed:mapObjIndexed,map:map,curryN:curryN,converge:converge,identity:identity}=require("ramda"),digSafe=curryN(2,(e,i)=>ifElse(is(Array),map(identity),when(is(Object),mapObjIndexed(converge(digSafe(e),[identity,e]))))(i));module.exports=digSafe;

},{"ramda":87}],332:[function(require,module,exports){
const{curryN:curryN}=require("ramda"),digBase=require("./digBase"),Stop=require("./stop"),digStop=curryN(2,(r,e)=>Stop().ifStopThenElse(digBase(digStop,r))(e));module.exports=digStop;

},{"./digBase":329,"./stop":351,"ramda":87}],333:[function(require,module,exports){
const{mapObjIndexed:mapObjIndexed,unary:unary,curryN:curryN,values:values,is:is,ifElse:ifElse,pipe:pipe}=require("ramda"),each=curryN(2,(e,a)=>ifElse(is(Array),pipe(mapObjIndexed(unary(e)),values),mapObjIndexed(unary(e)))(a));module.exports=each;

},{"ramda":87}],334:[function(require,module,exports){
const{flip:flip}=require("ramda"),each=require("./each"),eachFlip=flip(each);module.exports=eachFlip;

},{"./each":333,"ramda":87}],335:[function(require,module,exports){
const{mapObjIndexed:mapObjIndexed,binary:binary,curryN:curryN,ifElse:ifElse,is:is,pipe:pipe,values:values}=require("ramda"),eachIndexed=curryN(2,(e,a)=>ifElse(is(Array),pipe(mapObjIndexed(binary(e)),values),mapObjIndexed(binary(e)))(a));module.exports=eachIndexed;

},{"ramda":87}],336:[function(require,module,exports){
const{flip:flip}=require("ramda"),eachIndexed=require("./eachIndexed");module.exports=flip(eachIndexed);

},{"./eachIndexed":335,"ramda":87}],337:[function(require,module,exports){
const{toPairs:toPairs}=require("ramda");module.exports=toPairs;

},{"ramda":87}],338:[function(require,module,exports){
const{all:all}=require("ramda");module.exports=all;

},{"ramda":87}],339:[function(require,module,exports){
const{path:path,split:split,curryN:curryN}=require("ramda"),get=curryN(2,(r,t)=>path(split(".")(`${r}`),t));module.exports=get;

},{"ramda":87}],340:[function(require,module,exports){
const{flip:flip}=require("ramda"),get=require("./get"),getFlip=flip(get);module.exports=getFlip;

},{"./get":339,"ramda":87}],341:[function(require,module,exports){
const{__:__,curryN:curryN}=require("ramda"),getOr=require("./getOr"),getFlipOr=curryN(3,(r,e,t)=>getOr(r,t,e));module.exports=getFlipOr;

},{"./getOr":342,"ramda":87}],342:[function(require,module,exports){
const{pathOr:pathOr,split:split,curryN:curryN}=require("ramda"),getOr=curryN(3,(r,t,p)=>pathOr(r,split(".")(`${t}`),p));module.exports=getOr;

},{"ramda":87}],343:[function(require,module,exports){
const{pathSatisfies:pathSatisfies,split:split,curryN:curryN,not:not,pipe:pipe,isNil:isNil}=require("ramda"),has=curryN(2,(i,s)=>pathSatisfies(pipe(isNil,not),split(".")(`${i}`),s));module.exports=has;

},{"ramda":87}],344:[function(require,module,exports){
const{pipe:pipe,not:not,isEmpty:isEmpty}=require("ramda"),isAny=pipe(isEmpty,not);module.exports=isAny;

},{"ramda":87}],345:[function(require,module,exports){
const{toLower:toLower}=require("ramda");module.exports=toLower;

},{"ramda":87}],346:[function(require,module,exports){
const{__:__,is:is,map:map,isNil:isNil,reject:reject,join:join,pipe:pipe,when:when,curryN:curryN,not:not,converge:converge,identity:identity}=require("ramda"),get=require("./get"),getOr=require("./getOr"),getFlip=require("./getFlip"),digStop=require("./digStop"),Stop=require("./stop"),parseStringWhenNumber=curryN(2,(e,r)=>{let i=!1;return e.startsWith("+")&&(i=!0,e=e.slice(1)),i?+get(e,r):get(e,r)}),morph=curryN(2,(e,r)=>digStop(e=>{if(is(String,e)){let i=parseStringWhenNumber(e,r);if(not(isNil(i)))return Stop(i)}else{if(!is(Array,e))return e;{let i=reject(isNil)(map(parseStringWhenNumber(__,r),e)).join(" ");if(i)return Stop(i)}}},e));module.exports=morph;

},{"./digStop":332,"./get":339,"./getFlip":340,"./getOr":342,"./stop":351,"ramda":87}],347:[function(require,module,exports){
const{pipe:pipe,reject:reject,map:map,curryN:curryN,when:when,either:either,is:is}=require("ramda"),rejectDeep=curryN(2,(e,r)=>when(either(is(Array),is(Object)),pipe(reject(e),map(rejectDeep(e))))(r));module.exports=rejectDeep;

},{"ramda":87}],348:[function(require,module,exports){
const{isEmpty:isEmpty,trim:trim,pipe:pipe,when:when,is:is}=require("ramda"),rejectDeep=require("./rejectDeep"),rejectDeepEmpty=rejectDeep(pipe(when(is(String),trim),isEmpty));module.exports=rejectDeepEmpty;

},{"./rejectDeep":347,"ramda":87}],349:[function(require,module,exports){
const{pipe:pipe,values:values,length:length}=require("ramda"),size=pipe(values,length);module.exports=size;

},{"ramda":87}],350:[function(require,module,exports){
const{keys:keys,length:length,is:is,isNil:isNil,last:last}=require("ramda"),get=require("./get"),has=require("./has");function Stack(t){if(!(this instanceof Stack))return new Stack(t);let e=[];return this.value=function(t,s={}){if(t){let a={data:t,_t:(new Date).getTime(),meta:s,index:e.length};return e.push(a),a}return get("data",last(e))},this.values=function(){return e},this.isStack=(t=>has("constructor",t)&&t.constructor.name===this.constructor.name),t&&this.value(t),this}module.exports=Stack;

},{"./get":339,"./has":343,"ramda":87}],351:[function(require,module,exports){
const{ifElse:ifElse,when:when}=require("ramda"),has=require("./has");function Stop(t){if(!(this instanceof Stop))return new Stop(t);let o=t;return this.constructor=Stop,this.Then=(t=>t?t.Then():o),this.isStop=(t=>has("constructor",t)&&t.constructor.name===this.constructor.name),this.ifStopThen=(t=>when(Stop().isStop,Stop().Then)(t)),this.ifStopThenElse=(t=>ifElse(Stop().isStop,Stop().Then,t)),this}module.exports=Stop;

},{"./has":343,"ramda":87}],352:[function(require,module,exports){
const{__:__,curryN:curryN}=require("ramda"),stringify=curryN(3,JSON.stringify)(__,null,2);module.exports=stringify;

},{"ramda":87}],353:[function(require,module,exports){
const{times:times,flip:flip}=require("ramda");module.exports=flip(times);

},{"ramda":87}],354:[function(require,module,exports){
const{toUpper:toUpper}=require("ramda");module.exports=toUpper;

},{"ramda":87}],355:[function(require,module,exports){
module.exports={entries:require("./functions/entries"),dig:require("./functions/dig"),digFlip:require("./functions/digFlip"),digStop:require("./functions/digStop"),digSafe:require("./functions/digSafe"),each:require("./functions/each"),eachFlip:require("./functions/eachFlip"),eachIndexed:require("./functions/eachIndexed"),eachIndexedFlip:require("./functions/eachIndexedFlip"),every:require("./functions/every"),rejectDeep:require("./functions/rejectDeep"),rejectDeepEmpty:require("./functions/rejectDeepEmpty"),timesFlip:require("./functions/timesFlip"),size:require("./functions/size"),isAny:require("./functions/isAny"),capture:require("./functions/capture"),captureAll:require("./functions/captureAll"),captureAs:require("./functions/captureAs"),captureAllAs:require("./functions/captureAllAs"),get:require("./functions/get"),getFlip:require("./functions/getFlip"),getOr:require("./functions/getOr"),getFlipOr:require("./functions/getFlipOr"),has:require("./functions/has"),morph:require("./functions/morph"),stringify:require("./functions/stringify"),upper:require("./functions/upper"),lower:require("./functions/lower"),compact:require("./functions/compact"),Stop:require("./functions/stop"),Stack:require("./functions/stack")};

},{"./functions/capture":322,"./functions/captureAll":323,"./functions/captureAllAs":324,"./functions/captureAs":325,"./functions/compact":327,"./functions/dig":328,"./functions/digFlip":330,"./functions/digSafe":331,"./functions/digStop":332,"./functions/each":333,"./functions/eachFlip":334,"./functions/eachIndexed":335,"./functions/eachIndexedFlip":336,"./functions/entries":337,"./functions/every":338,"./functions/get":339,"./functions/getFlip":340,"./functions/getFlipOr":341,"./functions/getOr":342,"./functions/has":343,"./functions/isAny":344,"./functions/lower":345,"./functions/morph":346,"./functions/rejectDeep":347,"./functions/rejectDeepEmpty":348,"./functions/size":349,"./functions/stack":350,"./functions/stop":351,"./functions/stringify":352,"./functions/timesFlip":353,"./functions/upper":354}],356:[function(require,module,exports){
const{__:__,type:type,trim:trim,is:is,equals:equals,clone:clone,not:not,find:find,and:and}=require("ramda"),{eachIndexedFlip:eachIndexedFlip,digStop:digStop,getOr:getOr,get:get,has:has,lower:lower,Stop:Stop,each:each,eachIndexed:eachIndexed}=require("svpr"),properties={selector:["$s","$selector"],data:["$d","$data"],actions:["$a","$actions"],group:["$g","$group"],key:["$k","$key"]};function detectProperties(e){let t={anyOf:!1};return eachIndexedFlip(properties,(a,r)=>{t[r]={exists:!1,type:null};let i=find(has(__,e),a);and(i,not(t.anyOf))&&(t.anyOf=!0),t[r].exists=!!i,t[r].value=e[i],t[r].type=type(e[i])}),t}module.exports=function e(t,a,r=!1){return digStop((a,r)=>{if(is(Object,a)&&not(is(Array,a))){let{selector:r,data:i,actions:n,group:s,key:o,anyOf:l}=detectProperties(a);if(r.exists){let a=t.nut(r.value);if(n.exists)if(is(Array,n.value)){let e="";each(t=>{is(String,t)?e=`${e} ${t}`:is(Object,t)&&eachIndexed((t,a)=>{is(String,t)?e=`${e} ${t}(${a})`:is(Array,t)&&(e=`${e} ${t}(${a.join(")(")})`)},t)},n.value),a.nut(`@ ${e}`)}else is(String,n.value)&&a.nut(`@ ${n.value}`);if(i.exists){let t={};equals("Array",i.type)?t=get("value.0",i):equals("Object",i.type)&&(t=get("value",i)),a.each(a=>e(a,t))}return Stop(a.value())}return a}return is(Array,a)&&has("0",a)?Stop(t.nut(`${get("0",a)} @each text`).value()):is(String,a)?Stop(t.nut(a).value()):void 0},a)};

},{"ramda":87,"svpr":355}]},{},[1]);
