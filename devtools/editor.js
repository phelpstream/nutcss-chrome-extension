var app = new Vue({
  el: "#editor",
  data: {
    input: {
			"title": "title"
		},
    output: "",
    port: {},
    editor: null,
    viewer: null
  },
  mounted: function() {
    // Create a port with background page for continous message communication
    this.port = chrome.extension.connect({
      name: "nutcss" //Given a Name
    })

    var _this = this
    // Listen to messages from the background page
    this.port.onMessage.addListener(function(message) {
			console.log("received a message:", message);
      _this.viewer.set(message.content)
    })

    this.getDOM()

    new Clipboard(".clipboard")

    this.editor = new JSONEditor(document.getElementById("jsoneditor"), {
      mode: "code",
      modes: ["code", "form", "text", "tree", "view"], // allowed modes
      onError: function(err) {
        alert(err.toString())
      },
      onModeChange: function(newMode, oldMode) {
        console.log("Mode switched from", oldMode, "to", newMode)
			},
			onChange: function(){
				_this.update()
			}
		})
		
		this.editor.set(this.input)

    this.viewer = new JSONEditor(document.getElementById("jsonviewer"), {
      mode: "view",
      modes: ["tree", "view"]
    })
  },

  methods: {
    getDOM: function() {
      var message = {
        action: "getDOM",
        tabId: chrome.devtools.inspectedWindow.tabId,
        frame: this.input
      }
      chrome.extension.sendMessage(message)
    },

    update: function(e) {
			console.log("fires update");
			let json = this.editor.get()
			if(json){
				this.sendInput(json)
			} else {
				console.error(json);
			}
    },

    sendInput: _.debounce(function(json) {
      this.input = json
      this.getDOM()
    }, 350)
  }
})
