
// This sends an object to the background page 
// where it can be relayed to the inspected page
function sendToPage(message) {
    message.tabId = chrome.devtools.inspectedWindow.tabId;
    chrome.extension.sendMessage(message);
}

(function createChannel() {
    //Create a port with background page for continous message communication
    var port = chrome.extension.connect({
        name: "nutcss" //Given a Name
    });

    // Listen to messages from the background page
    port.onMessage.addListener(function (message) {
      document.querySelector('body').innerHTML = message.content;
      // port.postMessage(message);
    });

    
    sendToPage({action: "getDOM"});

}());

