var app = new Vue({
  el: "#editor",
  data: {
    // input: { title: "title" },
    input: `title: "title"`,
    output: "",
    port: {},
    env: "extension",
    editor: null,
    viewer: null
  },
  mounted: function() {
    var _this = this

    if (this.env === "extension") {
      // Create a port with background page for continous message communication
      this.port = chrome.extension.connect({
        name: "nutcss" //Given a Name
      })
      // Listen to messages from the background page
      this.port.onMessage.addListener(function(message) {
        _this.viewer.setValue(JSON.stringify(message.content, null, 2), -1)
      })
    }
    this.getDOM()

    this.editor = ace.edit("input")
    this.editor.setTheme("ace/theme/github")
    // this.editor.session.setMode("ace/mode/json")
    this.editor.session.setMode("ace/mode/yaml")
    this.editor.session.on("change", function(delta) {
      // console.log(_this.editor.session.getValue());
      _this.sendInput(_this.editor.session.getValue())
    })
    // this.editor.setValue(JSON.stringify(this.input, null, 2), -1)
    this.editor.setValue(this.input, -1)
    this.editor.setOptions({
      tabSize: 2,
      useSoftTabs: true,
      fontSize: "14pt"
    })
    // this.editor.resize()

    this.viewer = ace.edit("output")
    this.viewer.setTheme("ace/theme/dracula")
    this.viewer.session.setMode("ace/mode/json")
    this.viewer.setReadOnly(true)
    this.viewer.setOptions({
      tabSize: 2,
      useSoftTabs: true,
      fontSize: "14pt"
    })
    // this.viewer.resize()
  },

  methods: {
    getDOM: function(frame) {
      if (this.env === "extension") {
        var message = {
          action: "getDOM",
          tabId: chrome.devtools.inspectedWindow.tabId,
          frame: frame
        }
        chrome.extension.sendMessage(message)
      }
    },

    // update: function(e) {
    //   let json = this.editor.session.getValue()
    //   if (json) {
    //     this.sendInput(JSON.parse(json))
    //   } else {
    //     console.error(json)
    //   }
    // },

    sendInput: _.debounce(function(yaml) {
      try {
        this.input = yaml
        var json = YAML.parse(yaml)
        console.log(json)
        // json = JSON.parse(json)
        this.getDOM(json)
      } catch (error) {}
    }, 350)
  }
})
