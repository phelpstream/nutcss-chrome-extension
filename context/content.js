nutcss.extend({
  prototypes: nutcssFrame
})

chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
  if (message.action === "getDOM") {
    let $ = nutcss.load(document.documentElement.outerHTML)
    // var $ = trunks(document.documentElement.outerHTML, {
    //   rootUrl: window.location.protocol + "//" + window.location.host + "/" + window.location.pathname
    // })

    console.log("message.frame", message.frame);
    
    var json = $("html")
      .parse(message.frame)
      .value()
    
    console.log(json);
    var resp = {
      action: "sendDOM",
      content: json
    }

    chrome.extension.sendMessage(resp)
  }
})
